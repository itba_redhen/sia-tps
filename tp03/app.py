import math
import sys
import numpy as np
import csv
from config import Config
from perceptron import *


def main(argv):
    # We parse the config info
    config = Config.from_file()

    # We determine which exercise we are going to execute
    if config.exercise == '1.1' or config.exercise == '1.2' or config.exercise == '3.1':
        # Preparations for exercise
        data_matrix = list()
        # Obtaining training data from file
        with open('resources/exercise-' + config.exercise + '.tsv', 'r') as file:
            tsv_reader = csv.reader(file, delimiter='\t')
            for row in tsv_reader:
                data_matrix.append(row)
        # Formatting using numpy
        input_array = np.array([row[:-1] for row in data_matrix], dtype=int)
        output_array = np.array([row[-1] for row in data_matrix], dtype=int)
        output_array = output_array.reshape(len(output_array), math.ceil(np.size(output_array)/len(output_array)))
        # Opening file for plot
        with open('exercise-' + config.exercise + '-out.tsv', mode='w') as error_file:
            # Creating perceptron
            if not config.exercise == '3.1':
                p = Perceptron(config.learning_rate, config.max_iterations, fd=error_file)
            else:
                # Multi layer perceptron
                p = MultiLayerPerceptron(config.structure, config.learning_rate, config.max_iterations, fd=error_file)
            # Epochs loop
            for i in range(config.epochs):
                # Training
                p.train(input_array, output_array, i)
        pass
    elif config.exercise == '2':
        # Preparations for exercise
        input_array = list()
        output_array = list()
        # Obtaining input data from file
        with open('resources/exercise-2-input.tsv', 'r') as file:
            tsv_reader = csv.reader(file, delimiter='\t')
            for row in tsv_reader:
                input_array.append(row)
        # Obtaining output data from file
        with open('resources/exercise-2-output.tsv', 'r') as file:
            tsv_reader = csv.reader(file, delimiter='\t')
            for row in tsv_reader:
                output_array.append(row[0])
        # Formatting using numpy
        input_array = np.array(input_array, dtype=float)
        output_array = np.array(output_array, dtype=float)
        # Normalizing output TODO delete line if not appropiate
        output_array = normalize(output_array, -1, 1)
        # input_array = normalize(input_array, -1, 1)
        if config.k > 1:
            # We split training set into k parts
            split_input, split_output, k = split_k(input_array, output_array, config.k)
            # Linear perceptron
            # Creating perceptron for cross validation
            lp = LinearPerceptron(config.learning_rate, config.max_iterations)
            # We get best way to split with k-fold cross validation
            test_in, test_out, train_in, train_out = k_fold_cross_validation(lp, k, split_input, split_output)
        else:
            train_in = test_in = input_array
            train_out = test_out = output_array
        # Opening file for plot
        with open('exercise-2-linear-out.tsv', mode='w') as error_file, \
                open('exercise-2-linear-tvt-out.tsv', mode='w') as accuracy_file:
            # Creating perceptron for cross validation
            lp = LinearPerceptron(config.learning_rate, config.max_iterations, fd=error_file)
            # Epochs
            for e in range(config.epochs):
                # Training
                lp.train(train_in, train_out, e)
                train_error = lp.get_error(train_in, train_out)
                # Testing
                test_error = lp.get_error(test_in, test_out)
                accuracy_file.write(
                    str(e) + "\t" + "{:.2f}".format(train_error) + "\t" + "{:.2f}".format(test_error) + "\n"
                )
        # Non Linear perceptron
        if config.k > 1:
            # Creating perceptron for cross validation
            nlp = NonLinearPerceptron(config.learning_rate, config.max_iterations)
            # We get best way to split with k-fold cross validation
            test_in, test_out, train_in, train_out = k_fold_cross_validation(nlp, k, split_input, split_output)
        else:
            train_in = test_in = input_array
            train_out = test_out = output_array
        with open('exercise-2-non-linear-out.tsv', mode='w') as error_file, \
                open('exercise-2-non-linear-tvt-out.tsv', mode='w') as accuracy_file:
            # Creating perceptron
            nlp = NonLinearPerceptron(config.learning_rate, config.max_iterations, fd=error_file, beta=config.beta)
            # Epochs
            for e in range(config.epochs):
                # Training
                nlp.train(train_in, train_out, e)
                train_error = nlp.get_error(train_in, train_out)
                # Testing
                test_error = nlp.get_error(test_in, test_out)
                accuracy_file.write(
                    str(e) + "\t" + "{:.2f}".format(train_error) + "\t" + "{:.2f}".format(test_error) + "\n"
                )
        pass
    elif config.exercise == '3.2':
        # Preparations to hold digits
        digits = list()
        # Reading data from file
        with open('resources/exercise-3.2.txt', 'r') as file:
            # Digits are 5x7 so every 7 rows we get a new digit representation
            i = 0
            digit = list()
            # Looping through lines
            for line in file:
                # Split lines by spaces to get a number array (we do conversion to int later)
                row = line.split(' ')
                # Sanity check
                if not len(row) == 5:
                    raise ValueError('Not a valid digits file')
                # Appending this row of the digit matrix
                for number in row:
                    digit.append(number)
                # Updating counter
                i += 1
                # Once it reaches 7 it mean this digit is done
                if i == 7:
                    # We add the digit to the list of digits
                    digits.append(digit)
                    # Reset digit for next iteration as well as counter
                    digit = list()
                    i = 0
            # We make a sanity check
            if not i == 0:
                raise ValueError('Not a valid digits file')
            # Formatting to numpy arrays
            digits = np.array(digits, dtype=int)
            # Creating output array (we are assuming digits start from 0 and increase by 1)
            parity = list()
            i = 0
            while i < len(digits):
                if i % 2 == 0:
                    parity.append(1)
                else:
                    parity.append(-1)
                i += 1
            # Formatting to numpy array
            parity = np.array(parity)
            if config.k > 1:
                # Split array
                split_input, split_output, k = split_k(digits, parity, config.k)
                # Creating perceptron for cross validation
                p = MultiLayerPerceptron(config.structure, config.learning_rate, config.max_iterations)
                # We get best way to split with k-fold cross validation
                test_in, test_out, train_in, train_out = k_fold_cross_validation(p, k, split_input, split_output)
            else:
                train_in = test_in = digits
                train_out = test_out = parity
            # Opening file for plotting
            with open('exercise-3.2-out.tsv', mode='w') as error_file, \
                    open('exercise-3.2-accuracy-out.tsv', mode='w') as accuracy_file:
                # Calling to multi-layer perceptron
                p = MultiLayerPerceptron(config.structure, config.learning_rate, config.max_iterations, fd=error_file)
                for e in range(config.epochs):
                    # Training
                    p.train(train_in, train_out, e)
                    train_accuracy = p.accuracy(train_in, train_out, config.min_error)
                    # Testing
                    test_accuracy = p.accuracy(test_in, test_out, config.min_error)
                    print("Train accuracy in epoch:", train_accuracy)
                    print("Test accuracy in epoch:", test_accuracy)
                    accuracy_file.write(
                        str(e) + "\t" + "{:.2f}".format(train_accuracy) + "\t" + "{:.2f}".format(test_accuracy) + "\n"
                    )
                pass
        pass
    else:
        # Not implemented
        raise NotImplemented
        pass


# Takes a vector and normalizes it to range [a,b]
def normalize(array: np.ndarray, a, b):
    return np.interp(array, (np.amin(array), np.amax(array)), (a, b))


def split_k(input_array: np.ndarray, output_array: np.ndarray, k: int):
    # We get a k that divides set evenly
    n = len(input_array)
    if k > n:
        # We change k to minimum
        k = 2
    while n % k != 0:
        k += 1
    # We split training set into k parts
    split_input = np.array_split(input_array, k)
    split_output = np.array_split(output_array, k)
    return split_input, split_output, k


def k_fold_cross_validation(p: Perceptron, k, split_input: np.ndarray, split_output: np.ndarray):
    # K-fold cross-validation
    accuracies = []
    for j in range(k):
        test_input, test_output, train_input, train_output = get_sets(split_input, split_output, k, j)
        # Now that we have our sets, we train the perceptron
        p.reset_weights()
        p.train(train_input, train_output, j)
        # We check metrics to determine if it's a better split TODO see if accuracy is a good metric or not
        accuracy = p.accuracy(test_input, test_output)
        accuracies.append(accuracy)
    # We get the average
    avg = sum(accuracies) / len(accuracies)
    # We get the j that is closest to the average
    best_j = 0
    j = 0
    while j < len(accuracies):
        # Distances to avg of each one
        x = abs(avg - accuracies[j])
        y = abs(avg - accuracies[best_j])
        # If new is closer we update
        if x < y:
            best_j = j
        j += 1
    # Once we are done, we found the best way to split the set
    return get_sets(split_input, split_output, k, best_j)


def get_sets(split_input, split_output, k, j):
    # Test set
    test_input = split_input[j]
    test_output = split_output[j]
    # Training set
    train_input = list()
    train_output = list()
    for i in range(k):
        if i != j:
            train_input.extend(split_input[i])
            train_output.extend(split_output[i])
    train_input = np.array(train_input)
    train_output = np.array(train_output)
    return test_input, test_output, train_input, train_output


if __name__ == '__main__':
    main(sys.argv[1:])
