from configparser import ConfigParser


class Config:
    config_path = 'app.conf'
    config_section = 'app-config'

    def __init__(self, exercise, epochs, structure, max_iterations: int, learning_rate: float,
                 min_error: float, beta: float, k: int):
        self.exercise = exercise
        self.epochs = epochs
        self.max_iterations = max_iterations
        self.learning_rate = learning_rate
        self.min_error = min_error
        self.structure = structure
        self.beta = beta
        self.k = k

    @classmethod
    def from_file(cls):
        # Open file for config parser
        f = open(cls.config_path, 'r')
        config = ConfigParser()
        config.read_file(f)
        # Close file
        f.close()
        # We go parameter by parameter initializing variables
        exercise = config[cls.config_section]['exercise']
        epochs = int(config[cls.config_section]['epochs'])
        if not epochs > 0:
            raise ValueError('Epochs must be a positive integer.')
        max_iterations = int(config[cls.config_section]['max_iterations'])
        if not max_iterations > 0:
            raise ValueError('Max iterations must be a positive integer.')
        learning_rate = float(config[cls.config_section]['learning_rate'])
        if not learning_rate > 0:
            raise ValueError('Learning rate must be a positive float.')
        min_error = float(config[cls.config_section]['min_error'])
        if not min_error > 0:
            raise ValueError('Min error must be a positive float.')
        structure = cls.__get_structure(config)
        beta = float(config[cls.config_section]['beta'])
        k = int(config[cls.config_section]['k'])
        if not k > 0:
            raise ValueError('K for k-fold cross validation must be a positive integer')
        return cls(exercise, epochs, structure, max_iterations, learning_rate, min_error, beta, k)

    @classmethod
    def __get_structure(cls, config):
        structure = config[cls.config_section]['hidden_structure']
        structure = structure.split(' ')
        number_of_hidden_layers = int(structure[0])
        structure_array = list()
        for number in structure:
            if int(number) <= 0:
                raise ValueError('Cant have negative or zero number of nodes in a hidden layer')
            structure_array.append(int(number))
        if len(structure_array) - 1 != number_of_hidden_layers:
            raise ValueError('Expected ' + str(number_of_hidden_layers) + ' An in hidden_structure')
        return structure_array
