import sys
import matplotlib.pyplot as plt
import pandas


def main(argv):
    # path of the file
    path = argv[0]

    # Define figure and ax
    fig, ax = plt.subplots(figsize=(8, 8))

    data = __get_data(path)

    if not data.empty:
        x = data["epoch"]
        y = data[["train_error", "test_error"]]
        labels = ["Train error", "Test error"]
        marker = "."

        # Plot both current and min error
        ax.plot(x, y, label=labels, marker=marker)
        ax.set_title('error over epoch')

        # display legend & titles
        # Legend
        ax.legend()
        # Axis labels
        ax.set_xlabel('Epoch')
        ax.set_ylabel('error')
        # Limits
        ax.set_xlim(xmin=0.0)
        ax.set_ylim(ymin=0.0)

        plt.show()
    else:
        raise Exception("File doesn't exist")


# Returns a dict of all players
def __get_data(path):
    try:
        data = pandas.read_csv(path, delimiter='\t',
                               names=[
                                   "epoch",
                                   "train_error",
                                   "test_error"
                               ])
    except FileNotFoundError:
        return pandas.DataFrame()
    return data


if __name__ == '__main__':
    main(sys.argv[1:])
