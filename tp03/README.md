# SIA TP 3 - Perceptrón Simple y Multicapa

## Dependencias Necesarias
* pip
* [pipenv](https://pipenv.pypa.io/en/latest/)

## Instalación y Uso
### Paso 0: Ir a la carpeta tp03
``
cd tp03
``

### Paso 1: Preparar el entorno
``
pipenv install
``

### Paso 2: TODO