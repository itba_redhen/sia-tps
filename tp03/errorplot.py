import sys
import matplotlib.pyplot as plt
import pandas


def main(argv):
    # path of the file
    path = argv[0]

    # Define figure and ax
    fig, ax = plt.subplots(nrows=2, figsize=(8, 8))

    data = __get_data(path)
    x = data["epoch"]
    y = data[["minError", "currentError"]]
    labels = ["Min Error", "Current Error"]
    marker = "."

    # Plot both current and min error
    ax[0].plot(x, y, label=labels, marker=marker)
    ax[0].set_title('Current and Min error per epoch')

    # Plot min error only
    y = data["minError"]
    labels = "Min Error"
    ax[1].plot(x, y, label=labels, marker=marker)
    ax[1].set_title('Min error per epoch')

    # display legend & titles
    for a in ax:
        # Legend
        a.legend()
        # Axis labels
        a.set_xlabel('Epoch')
        a.set_ylabel('Error')

    # set the spacing between subplots
    plt.subplots_adjust(left=0.1,
                        bottom=0.1,
                        right=0.9,
                        top=0.9,
                        wspace=0.4,
                        hspace=0.4)

    plt.show()


# Returns a dict of all players
def __get_data(path):
    try:
        data = pandas.read_csv(path, delimiter='\t',
                               names=[
                                   "epoch",
                                   "iteration",
                                   "currentError",
                                   "minError"
                               ])
    except FileNotFoundError:
        return pandas.DataFrame()
    return data


if __name__ == '__main__':
    main(sys.argv[1:])
