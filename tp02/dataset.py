import csv
import random
from items import *


class Dataset:
    weapons_path = "armas.tsv"
    boots_path = "botas.tsv"
    gauntlets_path = "guantes.tsv"
    breastplates_path = "pecheras.tsv"
    helmets_path = "cascos.tsv"

    def __init__(self):
        # We populate weapon info
        self.weapons = self.get_list_from_tsv(self.weapons_path)
        # We populate boot info
        self.boots = self.get_list_from_tsv(self.boots_path)
        # We populate gauntlet info
        self.gauntlets = self.get_list_from_tsv(self.gauntlets_path)
        # We populate breastplate info
        self.breastplates = self.get_list_from_tsv(self.breastplates_path)
        # We populate helmets info
        self.helmets = self.get_list_from_tsv(self.helmets_path)

    @staticmethod
    def get_list_from_tsv(path: str):
        ret = list()
        with open(path, 'r') as file:
            tsv_reader = csv.reader(file, delimiter="\t")
            for row in tsv_reader:
                if row[0] != 'id':
                    ret.append(row)
        return ret

    def get_random_weapon(self):
        return self.get_random_item(self.weapons)

    def get_random_boots(self):
        return self.get_random_item(self.boots)

    def get_random_gauntlets(self):
        return self.get_random_item(self.gauntlets)

    def get_random_breastplate(self):
        return self.get_random_item(self.breastplates)

    def get_random_helmet(self):
        return self.get_random_item(self.helmets)

    @staticmethod
    def get_random_item(source: list):
        idx = random.randint(0, len(source) - 1)
        row = source[idx]
        return Item(float(row[1]), float(row[2]), float(row[3]), float(row[4]), float(row[5]), int(row[0]))
