import sys
import math
import csv
import statistics
from config import Config
from dataset import Dataset
from players import *


def main(argv):
    # Initializing dataset
    print("Initializing dataset...")
    dataset = Dataset()
    print("Done.")
    # Let's parse the configuration
    print("Reading configuration file...")
    config = Config.from_file(dataset)
    print("Done.")
    # Creating initial population
    print("Creating initial population...")
    population = list()
    generation = 0
    size = config.population_size
    population_type = config.player_class
    for n in range(size):
        h = Player.get_random_height()
        weapon = dataset.get_random_weapon()
        boots = dataset.get_random_boots()
        helmet = dataset.get_random_helmet()
        breastplate = dataset.get_random_breastplate()
        gauntlet = dataset.get_random_gauntlets()
        new_player = population_type(h, weapon, boots, helmet, gauntlet, breastplate)
        population.append(new_player)
    print("Done.")
    # Initializing
    print("Initializing cutoff condition...")
    config.cutoff_method.start(population)
    print("Done.")
    # Opening tsv file for the graphs
    with open('generations.tsv', mode='w', newline='') as generations_tsv_file,\
            open('fitness.tsv', mode='w', newline='') as fitness_tsv_file:
        # Writer and headers for generations
        g_fields = ['gen', 'class', 'height', 'strength', 'agility', 'proficiency', 'resistance', 'health', 'fitness']
        generations_writer = csv.DictWriter(generations_tsv_file, delimiter='\t', fieldnames=g_fields)
        generations_writer.writeheader()
        # Writer and headers for fitness
        f_fields = ['gen', 'min', '1quartile', 'mean', 'median', '3quartile', 'max']
        fitness_writer = csv.DictWriter(fitness_tsv_file, delimiter='\t', fieldnames=f_fields)
        fitness_writer.writeheader()
    # Input gen 0 info to tsv file
    _write_population(generation, population)
    _write_fitness(generation, population)
    # If condition met we finish, otherwise we loop
    while not config.cutoff_method.should_end():
        print("Current Generation:", generation)
        print("Selecting parents...")
        # Selection of parents
        method_1_qty = int(math.floor(config.k * config.a))
        method_2_qty = config.k - method_1_qty

        parents_1 = config.selection_method_1.select(population, method_1_qty)
        parents_2 = config.selection_method_2.select(population, method_2_qty)

        parents = parents_1 + parents_2
        print("Done.")
        # Crossover between parents
        print("Breeding children...")
        children = config.crossover_method.get_children(parents)
        print("Done.")
        # Mutation
        print("Mutating children...")
        config.mutation_method.mutate(children)
        print("Done.")
        # New population is
        print("Selecting new generation...")
        population = config.implementation_method.next_generation(population, children)
        print("Done.")
        print("New population len is: " + str(len(population)))
        # We update cutoff method
        print("Updating cutoff condition...")
        config.cutoff_method.update(population)
        print("Done.")
        generation += 1
        # Input new generation to tsv file
        _write_population(generation, population)
        # Input new fitness info to tsv file
        _write_fitness(generation, population)

    # Cleaning up
    print("Obtaining best player...")
    best_player = population[0]
    for p in population:
        if p.fitness() > best_player.fitness():
            best_player = p
    print("Done.")
    print("Best player fitness is: " + str(best_player.fitness()))
    print("Best player items are:")
    print("Weapon:")
    print(best_player.weapon)
    print("Boots:")
    print(best_player.boots)
    print("Helmet:")
    print(best_player.helmet)
    print("Gauntlet:")
    print(best_player.gauntlet)
    print("Chest:")
    print(best_player.chest)
    print("Height:")
    print(str(best_player.height))


def _write_population(generation: int, population: list):
    print("Updating generation.tsv file...")
    with open('generations.tsv', mode='a', newline='') as tsv_file:
        g_fields = ['gen', 'class', 'height', 'strength', 'agility', 'proficiency', 'resistance', 'health', 'fitness']
        writer = csv.DictWriter(tsv_file, delimiter='\t', fieldnames=g_fields)
        for p in population:
            writer.writerow({'gen': generation,
                             'class': p.key,
                             'height': p.height,
                             'strength': p.strength(),
                             'agility': p.agility(),
                             'proficiency': p.proficiency(),
                             'resistance': p.resistance(),
                             'health': p.health(),
                             'fitness': p.fitness()})
    print("Done.")


def _write_fitness(generation: int, population: list):
    print("Updating fitness.tsv file...")

    # Local variables
    fitness_list = list()
    min_fitness = math.inf
    max_fitness = -math.inf
    total_fitness = 0
    # Iterating the population to fetch fitness info
    for p in population:
        f = p.fitness()
        fitness_list.append(f)
        min_fitness = min(min_fitness, f)
        max_fitness = max(max_fitness, f)
        total_fitness += f
    qt = statistics.quantiles(fitness_list)

    # Inserting info to tsv
    with open('fitness.tsv', mode='a', newline='') as tsv_file:
        f_fields = ['gen', 'min', '1quartile', 'mean', 'median', '3quartile', 'max']
        writer = csv.DictWriter(tsv_file, delimiter='\t', fieldnames=f_fields)
        writer.writerow({
            "gen": generation,
            "min": min_fitness,
            "1quartile": qt[0],
            "mean": total_fitness / len(fitness_list),
            "median": qt[1],
            "3quartile": qt[2],
            "max": max_fitness
        })
    print("Done.")


if __name__ == "__main__":
    main(sys.argv[1:])
