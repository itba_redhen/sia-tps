from abc import ABCMeta, abstractmethod
from selections import Selection


class Implementation(metaclass=ABCMeta):
    key = "None"

    def __init__(self, method_1: Selection, method_2: Selection, distribution: float):
        self.method_1 = method_1
        self.method_2 = method_2
        self.distribution = distribution
        pass

    # Based on population, new children, and config it returns the new population
    @abstractmethod
    def next_generation(self, current_generation: list, children: list):
        pass
