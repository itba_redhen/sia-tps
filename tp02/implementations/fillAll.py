import math
from implementations import Implementation


class FillAll(Implementation):
    key = "fill-all"

    def next_generation(self, current_generation: list, children: list):
        # N is the number of current generation
        b = self.distribution
        n = len(current_generation)

        # We going to use 2 methods
        method_1_qty = int(math.floor(n * b))
        method_2_qty = n - method_1_qty

        # We work on the union of both lists
        eligible_population = current_generation + children
        method_1 = self.method_1.select(eligible_population, method_1_qty)
        method_2 = self.method_2.select(eligible_population, method_2_qty)

        # We return the next generation
        return method_1 + method_2
