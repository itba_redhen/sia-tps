import math
from implementations import Implementation


class FillParent(Implementation):
    key = "fill-parent"

    def next_generation(self, current_generation: list, children: list):
        # K is the number of children and N the number of current generation
        k = len(children)
        n = len(current_generation)
        b = self.distribution

        # Next generation depends on k and n
        if k > n:
            # We going to select n players using 2 methods based on distribution
            method_1_qty = int(math.floor(n * b))
            method_2_qty = n - method_1_qty

            # We select only from children
            method_1 = self.method_1.select(children, method_1_qty)
            method_2 = self.method_2.select(children, method_2_qty)

            # We return the next generation
            return method_1 + method_2
        else:
            # In this case we only need to select from the non-children
            qty = n - k

            # And from this qty we going to use two different methods
            method_1_qty = int(math.floor(qty * b))
            method_2_qty = qty - method_1_qty

            # Which results in
            method_1 = self.method_1.select(current_generation, method_1_qty)
            method_2 = self.method_2.select(current_generation, method_2_qty)

            # We return the next generation
            return method_1 + method_2 + children
