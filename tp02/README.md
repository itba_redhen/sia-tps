# SIA TP 2 - Algoritmos Genéticos

## Dependencias Necesarias
* GNU make
* pip
* [pipenv](https://pipenv.pypa.io/en/latest/)

## Instalación y Uso
### Paso 0: Ir a la carpeta tp02
``
cd tp02
``

### Paso 1: Preparar el entorno
``
pipenv install
``

### Paso 2: Modificar archivo de configuraciones
Disponible desde:
``
app.conf
``

Descripción de los parametros en comentarios de dicho archivo

### Paso 3: Abrir el visualizador de graficos (Opcional)
``
pipenv run python fitnessLinePlot.py
``

### Paso 4: Correr el programa principal
``
pipenv run python app.py
``