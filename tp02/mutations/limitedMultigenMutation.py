from mutations import Mutation
from players import Player
from numpy import random


class LimitedMultigenMutation(Mutation):
    key = "limited_multigen"

    def mutate(self, children: list):

        # Storing amount of genes as local variable
        genes = Player.genes

        # Storing the dataset as local variable
        dataset = self.dataset

        # Storing mutation probability as local variable
        mutation_probability = self.mutation_probability

        # Iterate through each child
        for child in children:
            # Defines if mutate child or not
            mutate = random.uniform(0, 1)
            if mutate < mutation_probability:
                # define how many gens to mutate
                gen_amount_to_mutate = random.randint(1, genes + 1)
                # array with the gene_ids to mutate
                gens_to_mutate = random.choice(range(0, genes), size=gen_amount_to_mutate, replace=False)
                # iterate through those gens
                for gene_id in gens_to_mutate:
                    child.mutate(gene_id, dataset)
