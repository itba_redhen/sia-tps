from mutations import Mutation
from players import Player
from numpy import random


class CompleteMutation(Mutation):
    key = "complete"

    def mutate(self, children: list):

        # Storing amount of genes as local variable
        genes = Player.genes

        # Storing the dataset as local variable
        dataset = self.dataset

        # Storing mutation probability as local variable
        mutation_probability = self.mutation_probability

        # Iterate through each child
        for child in children:
            # Defines if mutate child or not
            mutate = random.uniform(0, 1)
            if mutate < mutation_probability:
                # Iterate through all the gene_ids
                for gene_id in range(0, genes):
                    child.mutate(gene_id, dataset)

        return
