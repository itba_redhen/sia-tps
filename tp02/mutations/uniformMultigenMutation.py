from mutations import Mutation
from players import Player
from numpy import random


class UniformMultigenMutation(Mutation):
    key = "uniform_multigen"

    def mutate(self, children: list):

        # Storing amount of genes as local variable
        genes = Player.genes

        # Storing the dataset as local variable
        dataset = self.dataset

        # Storing mutation probability as local variable
        mutation_probability = self.mutation_probability

        # Iterate through each child
        for child in children:
            # Iterate through each gene_id
            for gene_id in range(0, genes):
                # Defines if mutate gene or not
                mutate = random.uniform(0, 1)
                if mutate < mutation_probability:
                    child.mutate(gene_id, dataset)
