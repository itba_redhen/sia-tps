from abc import ABCMeta, abstractmethod
from dataset import Dataset


# Interface for Mutation, which holds mutate method
class Mutation(metaclass=ABCMeta):
    key = "None"

    # Stores dataset & mutation probability
    def __init__(self, dataset: Dataset, mutation_probability: float = 0.0):
        # Dataset where it fetches the item data
        self.dataset = dataset
        # Probability to mutate, each method treats it in a specific way
        self.mutation_probability = mutation_probability

    # For each child, it may mutate it (depends on mutation probability)
    # No return value (as it modifies children directly)
    @abstractmethod
    def mutate(self, children: list):
        pass
