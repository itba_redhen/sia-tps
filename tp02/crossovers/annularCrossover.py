from crossovers import Crossover
from players import Player
import math
import copy
from numpy import random


class AnnularCrossover(Crossover):
    key = "annular"

    def cross(self, parents: list):
        # Storing number of genes as local variable
        genes = Player.genes
        # Storing number length, which defines how many genes to cross
        length = math.ceil(genes / 2)
        # Number point which defines the starting point of the crossover
        point = random.randint(0, genes)

        # Create a copy of the input as a base
        # Not using deepcopy as the item & height is immutable
        children = [copy.copy(parents[0]), copy.copy(parents[1])]

        # iterate through the length value
        for r in range(0, length):
            # get the index to cross, point + range may be higher than max gene_id,
            # we get the module for such cases (so it loops and after the last, it goes to the first)
            gene_id = (point + r) % genes
            # Crossing children
            children[0].cross(gene_id, parents[1])
            children[1].cross(gene_id, parents[0])

        return children
