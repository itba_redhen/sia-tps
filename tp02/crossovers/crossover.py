from abc import ABCMeta, abstractmethod


class Crossover(metaclass=ABCMeta):
    key = "none"

    # Receives a list of parents, and returns a list of children
    def get_children(self, parents: list) -> list:
        # We get the length of the parents list as K
        k = len(parents)
        # We initialize the children
        children = list()
        # We cross parents
        idx = 0
        while idx < k:
            # First we get the father
            father = parents[idx]
            # We move the cursor
            idx += 1
            # If cursor is within limits we get mother normally
            if idx < k:
                mother = parents[idx]
                # And we get the kids from this parents
                kids = self.cross([father, mother])
            # If it's not that means we had an uneven amount of parents
            else:
                # So we use the first one as a mother
                mother = parents[0]
                # And we only consider the first kid they produce
                kids = list()
                kids.append(self.cross([father, mother])[0])
            # Finally we add the kids to the final list
            for kid in kids:
                children.append(kid)
            # and increase the cursor by 1
            idx += 1
        # Now we just return the list of children
        return children

    # receives a list of 2 players, returns a list of the 2 new players
    @abstractmethod
    def cross(self, parents: list) -> list:
        pass
