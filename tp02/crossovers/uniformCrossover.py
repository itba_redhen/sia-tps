from crossovers import Crossover
from players import Player
import copy
from numpy import random


class UniformCrossover(Crossover):
    key = "uniform"

    def __init__(self, probability: float = 0.5):
        # Probability defines the chance of crossing the current gene.
        # May not be 0.5
        self.probability = probability

    def cross(self, parents: list):

        # Storing number of genes as local variable
        genes = Player.genes

        # Storing the gene probability as local variable
        p = self.probability

        # Create a copy of the input as a base
        # Not using deepcopy as the item & height is immutable
        children = [copy.copy(parents[0]), copy.copy(parents[1])]

        # Iterating all genes of Player
        for gene_id in range(0, genes):
            # Value from 0 to 1, crosses if lower than p
            cross_current_gene = random.uniform(0, 1)
            if cross_current_gene < p:
                # Crossing children
                children[0].cross(gene_id, parents[1])
                children[1].cross(gene_id, parents[0])

        return children
