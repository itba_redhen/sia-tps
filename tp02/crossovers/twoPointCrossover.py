from crossovers import Crossover
from players import Player
import copy
from numpy import random


class TwoPointCrossover(Crossover):
    key = "two_point"

    def cross(self, parents: list):

        # Storing number of genes as local variable
        genes = Player.genes

        # Get the One point where it starts swapping genes
        one_point = random.randint(0, genes + 1)
        # Get the Two point where it stops swapping genes
        two_point = random.randint(one_point, genes + 1)

        # Create a copy of the input as a base
        # Not using deepcopy as the item & height is immutable
        children = [copy.copy(parents[0]), copy.copy(parents[1])]

        # Iterating through the genes
        for gene_id in range(0, genes):
            # Swaps only between the one_point-th gene and the two_point-th gene
            if one_point <= gene_id < two_point:
                # Crossing children
                children[0].cross(gene_id, parents[1])
                children[1].cross(gene_id, parents[0])

        return children
