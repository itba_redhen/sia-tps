from crossovers import Crossover
from players import Player
import copy
from numpy import random


class OnePointCrossover(Crossover):
    key = "one_point"

    def cross(self, parents: list):

        # Storing number of genes as local variable
        genes = Player.genes

        # Get the One point where it starts swapping genes
        # Returns value between 0 (Swap all) and 6 (Don't swap at all)
        one_point = random.randint(0, genes + 1)

        # Create a copy of the input as a base
        # Not using deepcopy as the item & height is immutable
        children = [copy.copy(parents[0]), copy.copy(parents[1])]

        # Iterating through the genes
        for gene_id in range(0, genes):
            # swaps from the one_point-th gene
            if gene_id >= one_point:
                # Crossing children
                children[0].cross(gene_id, parents[1])
                children[1].cross(gene_id, parents[0])

        return children
