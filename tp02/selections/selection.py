from abc import ABCMeta, abstractmethod


# Interface for Selector, which holds select method
class Selection(metaclass=ABCMeta):
    key = "None"

    # Selects k elements from a list of players and returns a list of k players
    @abstractmethod
    def select(self, players: list, k: int) -> list:
        pass
