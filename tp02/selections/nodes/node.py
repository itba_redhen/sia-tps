from players import Player


# Wrapper for (score, Player) tuple
class Node:

    def __init__(self, score, player: Player):
        self.score = score
        self.player = player

    # Methods to allow sorting
    def __eq__(self, other):

        if not isinstance(other, Node):
            return False

        return self.score == other.score

    def __hash__(self):
        return hash(self.score)

    def __lt__(self, other):
        if not isinstance(other, Node):
            return False

        return self.score < other.score
