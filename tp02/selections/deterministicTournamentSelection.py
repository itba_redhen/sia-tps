from selections import Selection
from selections.nodes import Node
from numpy import random


class DeterministicTournamentSelection(Selection):
    key = "deterministic_tournament"

    def __init__(self, tournament_size: int = 2):
        # Store the amount of tournament participants
        self.tournament_size = tournament_size

    def select(self, players: list, k: int):
        # We make sure k is not 0
        if k == 0:
            return list()
        # Storing the size of the player's list as local variable
        players_len = len(players)

        # storing tournament_size as local variable
        m = self.tournament_size

        # List with the selected players
        selected_players = list()

        # Select the best participant k times
        for i in range(0, k):
            # List of nodes with the participants
            tournament_participants = list()

            # Gathering all the participants
            for j in range(0, m):
                chosen_index = random.randint(0, players_len)
                chosen_player = players[chosen_index]
                tournament_participants.append(Node(chosen_player.fitness(), chosen_player))

            # Choosing the best participant
            best_participant = (max(tournament_participants)).player

            # Adding the best player to the result list
            selected_players.append(best_participant)

        return selected_players
