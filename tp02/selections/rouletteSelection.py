from selections import Selection
from selections.nodes import Node
from numpy import random


class RouletteSelection(Selection):
    key = "roulette"

    # Using random
    def select(self, players: list, k: int):
        # We make sure k is not 0
        if k == 0:
            return list()
        # Get the list of player nodes
        player_node_list = self._get_player_list(players)

        # Create sorted list with K random numbers from [0,1)
        roulette_list = self._get_r(k)

        # Select k players
        # Since roulette list & player list are sorted,
        # double list iteration would allow to add the player with
        # the lowest fitness that has a higher value than the roulette value
        selected_players = list()
        # Current player index
        cpi = 0
        # Current roulette index
        cri = 0
        while cri < k:
            if roulette_list[cri] <= player_node_list[cpi].score:
                # This player's fitness is higher than the roulette value, so we add it to the list
                # Also we keep cpi since the next roulette value may be lower than this player's fitness
                selected_players.append(player_node_list[cpi].player)
                cri += 1
            else:
                # This player's fitness is lower than the roulette value, so we skip it
                # since also the other roulette values are higher than this player's fitness
                cpi += 1

        return selected_players

    # Returns the list of Nodes to work with
    def _get_player_list(self, players):

        # get the total fitness for later
        total_fitness = sum(map(lambda p: p.fitness(), players))

        # Create list with the accumulated fitness for each player
        q = 0
        accumulated_fitness_players = list()
        for player in players:
            q += player.fitness()
            current_accumulated_fitness = q / total_fitness
            accumulated_fitness_players.append(Node(current_accumulated_fitness, player))

        # Normalization to have values between [0,1) happens during loop
        return accumulated_fitness_players

    # Returns sorted list of k values for roulette
    @staticmethod
    def _get_r(k: int):
        return sorted(random.uniform(0, 1, k))
