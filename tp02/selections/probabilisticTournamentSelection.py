from selections import Selection
from selections.nodes import Node
from numpy import random


class ProbabilisticTournamentSelection(Selection):
    key = "probabilistic_tournament"

    def __init__(self, threshold: float = 0.5):
        # Threshold value, should be within [0.5,1.0]
        self.threshold = threshold

    def select(self, players: list, k: int):
        # We make sure k is not 0
        if k == 0:
            return list()
        # Storing the size of the player's list as local variable
        players_len = len(players)

        # Storing threshold as local variable
        threshold = self.threshold

        # List with the selected players
        selected_players = list()

        # Get the k players
        for i in range(0, k):

            # Selecting 2 participants from the player list
            participants = list()
            for j in range(0, 2):
                # Getting random index
                participant_index = random.randint(0, players_len)
                participant = players[participant_index]
                # Adding to participant list
                participants.append(Node(participant.fitness(), participant))

            # Getting value r from [0,1] that defines which participant wins
            r = random.uniform(0, 1)

            # Selecting the best participant according to r & threshold
            if r < threshold:
                # Select the most fit
                best_participant = (max(participants)).player
            else:
                # Select the less fit
                best_participant = (min(participants)).player

            selected_players.append(best_participant)

        return selected_players
