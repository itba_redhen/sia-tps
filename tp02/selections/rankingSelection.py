from selections import RouletteSelection
from selections.nodes import Node


class RankingSelection(RouletteSelection):
    key = "ranking"

    def __init__(self, _get_pseudo_aptitude=None):
        # Get rank as parameter to allow overwriting of function
        if _get_pseudo_aptitude is None or not callable(_get_pseudo_aptitude):
            self._get_pseudo_aptitude = self._get_pseudo_aptitude_default
        else:
            self._get_pseudo_aptitude = _get_pseudo_aptitude

    # Returns the list of Nodes to work with
    def _get_player_list(self, players):

        # Creates a list of (fitness, Player) tuples sorted by fitness in DESC order
        sorted_players = sorted(list(map(lambda p: Node(p.fitness(), p), players)), reverse=True)
        sorted_players_len = len(sorted_players)

        # Creates a new list by pseudo aptitude using the sorted list to get the rank for each player
        ranked_players = list()
        q = 0
        for i in range(0, sorted_players_len):
            # Q holds the accumulated pseudo_aptitude
            q += self._get_pseudo_aptitude(player=sorted_players[i].player, i=i+1, players=players)
            ranked_players.append(Node(q, sorted_players[i].player))

        # Normalize to have score between [0,1)
        for node in ranked_players:
            node.score /= q

        return ranked_players

    # Default function for get pseudo aptitude.
    # Receives the rank and the total elements and returns its according pseudo aptitude
    def _get_pseudo_aptitude_default(self, i, players: list, player=None):

        n = len(players)

        if i > n or i < 1:
            raise SyntaxError("i isn't a value between 1 and n")

        return (n - i) / n
