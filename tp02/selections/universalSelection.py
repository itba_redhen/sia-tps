from selections import RouletteSelection
from numpy import random


class UniversalSelection(RouletteSelection):
    key = "universal"

    # Returns sorted list of k values for roulette
    # Overrides the method
    @staticmethod
    def _get_r(k: int):
        # The random value r
        r = random.uniform(0, 1)
        # rj is the list holding the roulette values
        rj_list = list()
        for j in range(0, k):
            rj = (r+j) / k
            rj_list.append(rj)

        return rj_list
