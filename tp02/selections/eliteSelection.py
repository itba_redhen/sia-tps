from selections import Selection
from selections.nodes import Node
import math


class EliteSelection(Selection):
    key = "elite"

    # Returns the k players with highest fitness
    def select(self, players: list, k: int):
        # We make sure k is not 0
        if k == 0:
            return list()
        # Creates a list of (fitness, Player) tuples sorted by fitness in DESC order
        sorted_players = sorted(list(map(lambda p: Node(p.fitness(), p), players)), reverse=True)

        players_len = len(players)
        selected_players = list()
        for i in range(0, len(sorted_players)):

            # Calculates the n(i) function.
            # Holds the number of times the current player is selected
            n = math.ceil((k-i) / players_len)

            # n is a decreasing value
            # Once it reaches 0, there's no need to stay inside the loop
            if n <= 0:
                break

            # Inserts the player n times into the list
            for j in range(0, n):
                selected_players.append(sorted_players[i].player)

        return selected_players
