from selections import RouletteSelection
from selections.nodes import Node
import math
import statistics


class BoltzmannSelection(RouletteSelection):
    key = "boltzmann"

    def __init__(self, initial_temperature: float = 1000, min_temperature: float = 100, k: float = 0.1,
                 _get_temperature=None, _get_pseudo_aptitude=None):
        # A generation counter to use in temperature functions
        self.generations = 0
        self.initial_temperature = initial_temperature
        self.min_temperature = min_temperature
        self.k = k
        # Get pseudo_aptitude as parameter to allow overwriting of function
        if _get_pseudo_aptitude is None or not callable(_get_pseudo_aptitude):
            self._get_pseudo_aptitude = self._get_pseudo_aptitude_default
        else:
            self._get_pseudo_aptitude = _get_pseudo_aptitude
        # If the temperature function is not defined, use default
        # Otherwise use the provided, raises error if the function provided is not callable
        if _get_temperature is None:
            self._get_temperature = self._get_temperature_default
        elif not callable(_get_temperature):
            raise SyntaxError('Temperature must be callable')
        else:
            self._get_temperature = _get_temperature

    # Returns the list of Nodes to work with
    def _get_player_list(self, players):
        # Get temperature for this generation
        t = self._get_temperature()
        # Getting the denominator value.
        # Calculates the average of the <e^(f(x) / T)>g
        # It creates a list with the e^(f(x) / T) values using map function and then the average
        pseudo_aptitude_denominator_value = statistics.mean(map(lambda p: math.exp(p.fitness() / t), players))

        # Create list with the accumulated fitness for each player
        q = 0
        accumulated_fitness_players = list()
        for player in players:
            q += self._get_pseudo_aptitude(player, pseudo_aptitude_denominator_value)
            accumulated_fitness_players.append(Node(q, player))

        # Normalize to have score between [0,1)
        for node in accumulated_fitness_players:
            node.score /= q

        # Update generation count for next temperature
        self.generations += 1

        # Return
        return accumulated_fitness_players

    def _get_pseudo_aptitude_default(self, player, denominator):

        # Store temperature as local variable
        t = self._get_temperature()
        # Calculating the numerator value
        pseudo_aptitude_numerator_value = math.exp(player.fitness() / t)
        # Calculating the division to get the final pseudo aptitude value
        pseudo_aptitude = pseudo_aptitude_numerator_value / denominator
        return pseudo_aptitude

    def _get_temperature_default(self):
        # Based on the formula shown in class
        x = math.exp(-1 * self.k * self.generations)
        return self.min_temperature + ((self.initial_temperature - self.min_temperature) * x)
