class Item:
    def __init__(self, strength, agility, proficiency, resistance, health, item_id):
        self.strength = strength
        self.agility = agility
        self.proficiency = proficiency
        self.resistance = resistance
        self.health = health
        self.item_id = item_id

    def __repr__(self):
        ret = "Id: " + str(self.item_id) + "\n"
        ret += "Strength: " + str(self.strength) + "\n"
        ret += "Agility: " + str(self.agility) + "\n"
        ret += "Proficiency: " + str(self.proficiency) + "\n"
        ret += "Resistance: " + str(self.resistance) + "\n"
        ret += "Health: " + str(self.health) + "\n"
        return ret

    def __eq__(self, other):
        if isinstance(other, Item):
            return self.item_id == other.item_id
        raise NotImplemented

    def __hash__(self):
        return hash((self.strength, self.agility, self.proficiency, self.resistance, self.health, self.item_id))
