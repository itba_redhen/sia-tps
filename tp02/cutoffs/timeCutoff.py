import time
from cutoffs import Cutoff


class TimeCutoff(Cutoff):
    key = "time"

    def __init__(self, cutoff_time: float):
        # Initial time holder
        self.initial_time = None
        # Cutoff value
        self.cutoff_time = cutoff_time

    def should_end(self):
        # If elapsed time is above the cutoff limit we should end
        elapsed_time = float(time.time() - self.initial_time)

        return elapsed_time > self.cutoff_time

    def start(self, initial_population: list):
        # We start the timer
        self.initial_time = time.time()

    def update(self, new_population: list):
        # In this scenario we dont need an update function
        pass
