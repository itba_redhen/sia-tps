import copy
from cutoffs import Cutoff
from players import Player


class StructureCutoff(Cutoff):
    key = "structure"

    def __init__(self, generations_limit: int, population_percentage: float = 0.5, genes_scope: int = 1):
        # The cutoff parameters are
        self.generations_limit = generations_limit
        if not 0 <= population_percentage <= 1:
            raise ValueError('Percentage should be represented with a real number between 0 and 1')
        self.population_percentage = population_percentage
        if not 0 < genes_scope <= Player.genes:
            raise ValueError('Gene scope should be a positive integer of up to ' + str(Player.genes))
        self.genes_scope = genes_scope
        # The variables to keep update are
        self.genes_popularity = [[], [], [], [], [], []]
        self.generations_since = 0

    def should_end(self):
        # If the number of generations since tangible change reaches the limit we end
        return self.generations_since >= self.generations_limit

    def start(self, initial_population: list):
        # No generations have gone since change cuz it's the first generation
        self.generations_since = 0
        # Then we get the genes popularity
        self.__get_genes_popularity(self.genes_popularity, initial_population)

    def __get_genes_popularity(self, genes_popularity: list, population: list):
        # We initialize the dictionaries to hold gene diversity for each gene
        gene_groups = dict()
        for gene_group_id in range(Player.genes):
            gene_groups[gene_group_id] = dict()
        # Now we go through our players
        for player in population:
            # Then for each gene we decided to consider
            for gene_group_id in range(Player.genes):
                # We update that gene count in the group
                self.__update_gene(gene_groups[gene_group_id], self.__get_gene(player, gene_group_id))
        # Now for each gene we are considering
        for gene_group_id in range(Player.genes):
            # We get the dictionary that has the Gene -> Number of people with that gene information
            gene_info = gene_groups[gene_group_id]
            # Now that we have this, we look for the gene with the highest number of people carrying it
            max_usage = 0
            most_popular = None
            for gene in gene_info:
                if gene_info[gene] > max_usage:
                    most_popular = gene
                    max_usage = gene_info[gene]
            # Now that we have the most popular gene, we save it alongside the percentage of population using it
            genes_popularity[gene_group_id] = [most_popular, max_usage / len(population)]

    @staticmethod
    def __update_gene(group: dict, gene):
        if gene in group:
            group[gene] += 1
        else:
            group[gene] = 1

    @staticmethod
    def __get_gene(player: Player, group_id: int):
        if group_id == 0:
            return player.weapon
        if group_id == 1:
            return player.boots
        if group_id == 2:
            return player.helmet
        if group_id == 3:
            return player.gauntlet
        if group_id == 4:
            return player.chest
        if group_id == 5:
            return player.height
        raise ValueError('Invalid group id.')

    def update(self, new_population: list):
        # Auxiliary genes popularity array
        new_genes_popularity = [[], [], [], [], [], []]
        # Now we populate the array with the new info
        self.__get_genes_popularity(new_genes_popularity, new_population)
        # Now we determine if there has been enough change
        genes_changed = 0
        for gene_group in range(Player.genes):
            # Previous most popular gene was
            old_gene = self.genes_popularity[gene_group]
            # The new most popular gene is
            new_gene = new_genes_popularity[gene_group]
            # If the genes are different, then that means there was a popularity shift
            # If the genes are the same, but the percentage of people with that gene is low then it's fine
            if old_gene[0] != new_gene[0] or new_gene[1] < self.population_percentage:
                genes_changed += 1
        # Now we see how many genes remained the same
        same_genes = Player.genes - genes_changed
        # If there are more genes that remained the same than our limit then we add to counter
        if same_genes >= self.genes_scope:
            self.generations_since += 1
        else:
            # Otherwise we reset it
            self.generations_since = 0
        # We update the genes popularity array
        for gene in range(Player.genes):
            self.genes_popularity[gene] = new_genes_popularity[gene]
