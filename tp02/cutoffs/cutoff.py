from abc import ABCMeta, abstractmethod


class Cutoff(metaclass=ABCMeta):
    # Returns True if cutoff state has been achieved
    @abstractmethod
    def should_end(self):
        pass

    # Initializes relevant variables according to implementation
    @abstractmethod
    def start(self, initial_population: list):
        pass

    # Updates relevant variables according to implementation
    @abstractmethod
    def update(self, new_population: list):
        pass
