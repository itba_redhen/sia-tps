from cutoffs import Cutoff


class AcceptableSolutionCutoff(Cutoff):
    key = "acceptable solution"

    def __init__(self, acceptable_solution: float):
        # Acceptable solution
        self.acceptable_solution = acceptable_solution
        # Current solution
        self.current_solution = None

    def should_end(self):
        # We end if our current solution is better or equal to our acceptable one
        return self.current_solution >= self.acceptable_solution

    def start(self, initial_population: list):
        # Initial solution
        self.current_solution = self.get_best_fitness(initial_population)

    def update(self, new_population: list):
        # We update current solution with new best score
        self.current_solution = self.get_best_fitness(new_population)

    @staticmethod
    def get_best_fitness(population: list):
        # We loop through the players
        current_best = 0
        for player in population:
            score = player.fitness()
            # And save the best score
            if score > current_best:
                current_best = score
        # Which we return
        return current_best
