from cutoffs import Cutoff


class GenerationCutoff(Cutoff):
    key = "generation"

    def __init__(self, cutoff_value: int):
        # Initial generation
        self.current_generation = 0
        # Max generation
        self.cutoff_generation = cutoff_value

    def should_end(self):
        # We end if this generation is the cutoff generation or bigger
        return self.current_generation >= self.cutoff_generation

    def start(self, initial_population: list):
        # Initial generation is called generation 0
        self.current_generation = 0

    def update(self, new_population: list):
        # We update generation number
        self.current_generation += 1
