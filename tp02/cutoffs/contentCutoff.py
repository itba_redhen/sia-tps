from cutoffs import Cutoff


class ContentCutoff(Cutoff):
    key = "content"

    def __init__(self, cutoff_value: int, epsilon: float = 0.001):
        # Limit on generations
        self.cutoff_value = cutoff_value
        # Epsilon on what counts as significant change
        self.epsilon = epsilon
        # Generations since same fitness
        self.generations_since = 0
        # Previous fitness
        self.previous_fitness = None

    def should_end(self):
        # We return if the number of generations since last change is bigger or equal to our cutoff value
        return self.generations_since >= self.cutoff_value

    def start(self, initial_population: list):
        # Initial solution
        self.previous_fitness = self.get_best_fitness(initial_population)
        self.generations_since = 0

    def update(self, new_population: list):
        # We update current solution with new best score
        new_solution = self.get_best_fitness(new_population)
        # We see if new solution changed significantly
        if abs(new_solution - self.previous_fitness) < self.epsilon:
            self.generations_since += 1
        else:
            self.previous_fitness = new_solution
            self.generations_since = 0

    @staticmethod
    def get_best_fitness(population: list):
        # We loop through the players
        current_best = 0
        for player in population:
            score = player.fitness()
            # And save the best score
            if score > current_best:
                current_best = score
        # Which we return
        return current_best
