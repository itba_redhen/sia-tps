import matplotlib.pyplot as plt
import matplotlib.animation as animation
import time
import csv

# figure for fitness
fitness_fig = plt.figure()
# figure for diversity
diversity_fig = plt.figure(figsize=(12, 8))
# Interval of update (milliseconds)
fitness_interval = 1000
diversity_interval = 2000


# Main function that calls the graph
def main():
    # Assignation to prevent garbage collection
    fitness_ani = _fitness_graph()
    diversity_ani = _diversity_graph()
    plt.show()


def _fitness_graph():
    return animation.FuncAnimation(fitness_fig, _fitness_animate, interval=fitness_interval)


def _diversity_graph():
    return animation.FuncAnimation(diversity_fig, _diversity_animate, interval=diversity_interval)


def _fitness_animate(i):
    # Clear previous input
    fitness_fig.clf()
    fitness_ax = fitness_fig.add_subplot(1, 1, 1)
    # Get a dict with the raw data
    raw_data = _get_data()
    # Only show graph if there are enough rows
    if len(raw_data) > 0:
        # Separate the fitness for each generation
        max_gen = int(raw_data[-1]["gen"])
        # Create a list of lists for each generation
        fitness_per_generation = [list() for _ in range(max_gen + 1)]
        for player in raw_data:
            if player["gen"] is not None:
                gen = int(player["gen"])
                fitness_per_generation[gen].append(float(player["fitness"]))

        fitness_per_generation_len = len(fitness_per_generation)

        # Building Violin plot
        fitness_ax.violinplot(fitness_per_generation,
                              showmeans=True,
                              showmedians=True)
        # Title
        fitness_ax.set_title('Fitness per generation')

        # Setting axis labels & ticks
        # adding horizontal grid lines
        fitness_ax.yaxis.grid(True)
        fitness_ax.set_xticks([y + 1 for y in range(fitness_per_generation_len)])
        # Axis labels
        fitness_ax.set_xlabel('Generation')
        fitness_ax.set_ylabel('Fitness')

        # Labels for each generation
        labels = [str(gen) for gen in range(fitness_per_generation_len)]
        plt.setp(fitness_ax, xticks=[y + 1 for y in range(fitness_per_generation_len)],
                 xticklabels=labels)


def _diversity_animate(i):
    # Stat multipliers to normalize values
    stat_multiplier = {
        "fitness": 1,
        "height": 10,
        "strength": 0.5,
        "agility": 50,
        "proficiency": 75,
        "resistance": 50,
        "health": 0.5
    }
    # Labels for x axis
    spoken_label = [
        "fitness\n(x{:.1f})".format(stat_multiplier["fitness"]),
        "height\n(x{:.1f})".format(stat_multiplier["height"]),
        "strength\n(x{:.1f})".format(stat_multiplier["strength"]),
        "agility\n(x{:.1f})".format(stat_multiplier["agility"]),
        "proficiency\n(x{:.1f})".format(stat_multiplier["proficiency"]),
        "resistance\n(x{:.1f})".format(stat_multiplier["resistance"]),
        "health\n(x{:.1f})".format(stat_multiplier["health"])
    ]
    # Clear previous input
    diversity_fig.clf()
    # Get a dict with the raw data
    raw_data = _get_data()
    # Only show graph if there are enough rows
    if len(raw_data) > 0:
        # Separate the fitness for each generation
        max_gen_str = raw_data[-1]["gen"]
        max_gen = int(max_gen_str)
        # Create a list of lists for each generation holding stats
        player_stats_last_gen = list()
        stats_in_last_gen = {
            "fitness": list(),
            "height": list(),
            "strength": list(),
            "agility": list(),
            "proficiency": list(),
            "resistance": list(),
            "health": list(),
        }
        for player in raw_data:
            if int(player["gen"]) == max_gen:
                # Repeat the first one on the end to "close" the shape
                player_stats_last_gen.append([
                    float(player["fitness"]) * stat_multiplier["fitness"],
                    float(player["height"]) * stat_multiplier["height"],
                    float(player["strength"]) * stat_multiplier["strength"],
                    float(player["agility"]) * stat_multiplier["agility"],
                    float(player["proficiency"]) * stat_multiplier["proficiency"],
                    float(player["resistance"]) * stat_multiplier["resistance"],
                    float(player["health"]) * stat_multiplier["health"]
                ])
                stats_in_last_gen["fitness"].append(float(player["fitness"]) * stat_multiplier["fitness"])
                stats_in_last_gen["height"].append(float(player["height"]) * stat_multiplier["height"])
                stats_in_last_gen["strength"].append(float(player["strength"]) * stat_multiplier["strength"])
                stats_in_last_gen["agility"].append(float(player["agility"]) * stat_multiplier["agility"])
                stats_in_last_gen["proficiency"].append(float(player["proficiency"]) * stat_multiplier["proficiency"])
                stats_in_last_gen["resistance"].append(float(player["resistance"]) * stat_multiplier["resistance"])
                stats_in_last_gen["health"].append(float(player["health"]) * stat_multiplier["health"])

        # Plot only the last gen
        # Having multiple at the same time makes the screen too convoluted
        diversity_ax = diversity_fig.add_subplot(1, 2, 1)
        # Adding each line
        for p in player_stats_last_gen:
            diversity_ax.plot(spoken_label, p)
        # Setting title & axis titles
        # Subplot title
        subplot_title = "Stat diversity | Generation " + str(max_gen)
        diversity_ax.set(xlabel='Stat', ylabel='Value', title=subplot_title)
        # adding horizontal grid lines
        diversity_ax.yaxis.grid(True)

        diversity_ax = diversity_fig.add_subplot(1, 2, 2)
        diversity_ax.violinplot(stats_in_last_gen.values(),
                                showmeans=True,
                                showmedians=True)

        # Additional graph to display stat distribution
        # Title
        diversity_ax.set_title('Stat distribution | Generation ' + str(max_gen))

        # Setting axis labels & ticks
        # adding horizontal grid lines
        spoken_label_len = len(spoken_label)
        diversity_ax.yaxis.grid(True)
        diversity_ax.set_xticks([y + 1 for y in range(spoken_label_len)])
        # Axis labels
        diversity_ax.set_xlabel('Stat')
        diversity_ax.set_ylabel('Value')

        # Labels for each generation
        plt.setp(diversity_ax, xticks=[y + 1 for y in range(spoken_label_len)],
                 xticklabels=spoken_label)

        # Since we only plot the last one, we save the previous ones
        output_dir = "out"
        mkdir_p(output_dir)
        diversity_fig.savefig("{dir:s}/diversity_time{time:s}_gen{gen:03n}.png".format(
            dir=output_dir,
            time=time.strftime("%H:%M", time.localtime()),
            gen=max_gen
        ))


# Returns a dict of all players
def _get_data():
    path = "generations.tsv"
    ret = list()
    try:
        with open(path, 'r') as file:
            tsv_reader = csv.DictReader(file, delimiter="\t")
            for row in tsv_reader:
                ret.append(row)
    finally:
        return ret


# Aux method to create a directory
def mkdir_p(my_path):
    """Creates a directory. equivalent to using mkdir -p on the command line"""

    from errno import EEXIST
    from os import makedirs, path

    try:
        makedirs(my_path)
    except OSError as exc:  # Python >2.5
        if exc.errno == EEXIST and path.isdir(my_path):
            pass
        else:
            raise


# Main function, wrapper for main()
if __name__ == "__main__":
    main()
