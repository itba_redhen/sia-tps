from players import Player


class Archer(Player):
    key = 'archer'

    def fitness(self):
        return 0.9 * self.attack() + 0.1 * self.defense()
