from players import Player


class Warrior(Player):
    key = 'warrior'

    def fitness(self):
        return 0.6 * self.attack() + 0.6 * self.defense()
