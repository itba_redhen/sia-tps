from players import Player


class Tank(Player):
    key = 'tank'

    def fitness(self):
        return 0.3 * self.attack() + 0.8 * self.defense()
