from players import Player


class Rogue(Player):
    key = 'rogue'

    def fitness(self):
        return 0.8 * self.attack() + 0.3 * self.defense()
