import math
import random
from items import Item


class Player:
    genes = 6

    def __init__(self, height: float, weapon: Item, boots: Item, helmet: Item, gauntlet: Item, chest: Item):
        self.weapon = weapon
        self.boots = boots
        self.helmet = helmet
        self.gauntlet = gauntlet
        self.chest = chest
        self.height = height

    def strength(self):
        return 100 * math.tanh(0.01 * self._items_strength())

    def agility(self):
        return math.tanh(0.01 * self._items_agility())

    def proficiency(self):
        return 0.6 * math.tanh(0.01 * self._items_proficiency())

    def resistance(self):
        return math.tanh(0.01 * self._items_resistance())

    def health(self):
        return 100 * math.tanh(0.01 * self._items_health())

    def atm(self):
        x = 3 * self.height - 5
        return 0.7 - math.pow(x, 4) + math.pow(x, 2) + (self.height / 4)

    def dem(self):
        x = 2.5 * self.height - 4.16
        return 1.9 + math.pow(x, 4) - math.pow(x, 2) - ((3 * self.height) / 10)

    def attack(self):
        return (self.agility() + self.proficiency()) * self.strength() * self.atm()

    def defense(self):
        return (self.resistance() + self.proficiency()) * self.health() * self.dem()

    def fitness(self):
        pass

    @staticmethod
    def get_random_height():
        height = random.uniform(1.3, 2)
        return round(height, 2)

    def _items_strength(self):
        ret = self.weapon.strength + self.boots.strength + self.gauntlet.strength \
              + self.chest.strength + self.helmet.strength
        return ret

    def _items_agility(self):
        ret = self.weapon.agility + self.boots.agility + self.gauntlet.agility \
              + self.chest.agility + self.helmet.agility
        return ret

    def _items_proficiency(self):
        ret = self.weapon.proficiency + self.boots.proficiency + self.gauntlet.proficiency \
              + self.chest.proficiency + self.helmet.proficiency
        return ret

    def _items_resistance(self):
        ret = self.weapon.resistance + self.boots.resistance + self.gauntlet.resistance \
              + self.chest.resistance + self.helmet.resistance
        return ret

    def _items_health(self):
        ret = self.weapon.health + self.boots.health + self.gauntlet.health \
              + self.chest.health + self.helmet.health
        return ret

    def __key(self):
        return self.weapon, self.boots, self.helmet, self.gauntlet, self.chest, self.height

    def __eq__(self, other):
        if isinstance(other, Player):
            return self.__key() == other.__key()

    def __hash__(self):
        return hash(self.__key())

    # Gets the gen id and swaps with the genes of the other player
    def cross(self, gene_id: int, other):
        if not isinstance(other, Player):
            raise TypeError('"Other" parameter must be a Player')
        if gene_id >= self.genes or gene_id < 0:
            raise ValueError('"gen_id" is not recognized as valid')
        elif gene_id == 0:
            # Weapon swap condition
            self.weapon = other.weapon
        elif gene_id == 1:
            # Boots swap condition
            self.boots = other.boots
        elif gene_id == 2:
            # Helmet swap condition
            self.helmet = other.helmet
        elif gene_id == 3:
            # Gauntlet swap condition
            self.gauntlet = other.gauntlet
        elif gene_id == 4:
            # Chest swap condition
            self.chest = other.chest
        elif gene_id == 5:
            # Height swap condition
            self.height = other.height

    # Mutates the gene defined by gene_id with a value from dataset or random value for height
    def mutate(self, gene_id, dataset):
        if gene_id >= self.genes or gene_id < 0:
            raise ValueError('"gen_id" is not recognized as valid')
        elif gene_id == 0:
            # Weapon mutate condition
            self.weapon = dataset.get_random_weapon()
        elif gene_id == 1:
            # Boots mutate condition
            self.boots = dataset.get_random_boots()
        elif gene_id == 2:
            # Helmet mutate condition
            self.helmet = dataset.get_random_helmet()
        elif gene_id == 3:
            # Gauntlet mutate condition
            self.gauntlet = dataset.get_random_gauntlets()
        elif gene_id == 4:
            # Chest mutate condition
            self.chest = dataset.get_random_breastplate()
        elif gene_id == 5:
            # Height mutate condition
            self.height = Player.get_random_height()
            pass
