from players import *
from cutoffs import *
from implementations import *
from mutations import *
from crossovers import *
from selections import *
from configparser import ConfigParser
from dataset import Dataset


class Config:
    config_path = 'app.conf'
    config_section = 'app-config'

    def __init__(self, player_class, population_size: int, k: int, impl_method: Implementation,
                 cutoff_method: Cutoff, crossover_method: Crossover, mutation_method: Mutation,
                 selection_1: Selection, selection_2: Selection, a: float):
        self.player_class = player_class
        self.population_size = population_size
        self.k = k
        self.implementation_method = impl_method
        self.cutoff_method = cutoff_method
        self.crossover_method = crossover_method
        self.mutation_method = mutation_method
        self.selection_method_1 = selection_1
        self.selection_method_2 = selection_2
        self.a = a

    @classmethod
    def from_file(cls, dataset: Dataset):
        # Open file for config parser
        f = open(cls.config_path, 'r')
        config = ConfigParser()
        config.read_file(f)
        # Close file
        f.close()
        # We go parameter by parameter initializing variables
        player_class = cls.__get_player_class(config)
        population_size = int(config[cls.config_section]['population_size'])
        if not population_size > 0:
            raise ValueError('Population size must be a positive integer.')
        k = int(config[cls.config_section]['K'])
        if not k > 0:
            raise ValueError('K must be a positive integer.')
        a = float(config[cls.config_section]['A'])
        if not 0 <= a <= 1:
            raise ValueError('A must be a real number between 0 and 1')
        implementation_method = cls.__get_implementation(config)
        cutoff_method = cls.__get_cutoff(config)
        crossover_method = cls.__get_crossover(config)
        mutation_method = cls.__get_mutation(config, dataset)
        selection_method_1 = cls.__get_selection(config, 1)
        selection_method_2 = cls.__get_selection(config, 2)
        return cls(player_class, population_size, k, implementation_method, cutoff_method, crossover_method,
                   mutation_method, selection_method_1, selection_method_2, a)

    @classmethod
    def __get_player_class(cls, config: ConfigParser):
        # This method assumes the config has the info already
        key = config[cls.config_section]['player_class']
        # Now we look for the value we want
        if key.lower() == Warrior.key:
            return Warrior
        if key.lower() == Rogue.key:
            return Rogue
        if key.lower() == Archer.key:
            return Archer
        if key.lower() == Tank.key:
            return Tank
        # Otherwise invalid value
        raise ValueError('Invalid player class value on ' + cls.config_path)

    @classmethod
    def __get_implementation(cls, config: ConfigParser):
        # This method assumes the config has the info already
        key = config[cls.config_section]['implementation']
        b = float(config[cls.config_section]['B'])
        if not 0 <= b <= 1:
            raise ValueError('B must be a real number between 0 and 1')
        method_1 = cls.__get_selection(config, 3)
        method_2 = cls.__get_selection(config, 4)
        # Now we look for the value we want
        if key.lower() == FillAll.key:
            return FillAll(method_1, method_2, b)
        elif key.lower() == FillParent.key:
            return FillParent(method_1, method_2, b)
        # Otherwise invalid value
        raise ValueError('Invalid implementation method on ' + cls.config_path)

    @classmethod
    def __get_cutoff(cls, config: ConfigParser):
        # This method assumes the config has the info already
        key = config[cls.config_section]['cutoff']
        cutoff_limit_key = 'cutoff_limit'
        # Now we look for the value we want
        # ---------TIME---------
        if key.lower() == TimeCutoff.key:
            # This method requires time
            val = float(config[cls.config_section][cutoff_limit_key])
            if not val > 0:
                raise ValueError('Cutoff time must be bigger than 0 seconds.')
            return TimeCutoff(val)
        # ---------GENERATION---------
        elif key.lower() == GenerationCutoff.key:
            # This method requires a generation limit
            val = int(config[cls.config_section][cutoff_limit_key])
            if not val > 0:
                raise ValueError('Generation cutoff must be at least 1.')
            return GenerationCutoff(val)
        # ---------ACCEPTABLE SOLUTION---------
        elif key.lower() == AcceptableSolutionCutoff.key:
            # This method requires a fitness float
            val = float(config[cls.config_section][cutoff_limit_key])
            if not val > 0:
                raise ValueError('Acceptable solution must be bigger than 0.')
            return AcceptableSolutionCutoff(val)
        # ---------CONTENT---------
        elif key.lower() == ContentCutoff.key:
            # This method requires a generation limit
            val = int(config[cls.config_section][cutoff_limit_key])
            if not val > 0:
                raise ValueError('Generation cutoff must be at least 1.')
            # Optionally they can provide an epsilon
            aux = 'cutoff_epsilon'
            if aux in config[cls.config_section]:
                epsilon = float(config[cls.config_section][aux])
                if not epsilon > 0:
                    raise ValueError('Epsilon must be bigger than 0')
                return ContentCutoff(val, epsilon)
            return ContentCutoff(val)
        # ---------STRUCTURE---------
        elif key.lower() == StructureCutoff.key:
            # This method requires a generation limit
            val = int(config[cls.config_section][cutoff_limit_key])
            if not val > 0:
                raise ValueError('Generation cutoff must be at least 1.')
            # Optionally they can provide a population percentage
            percentage = None
            percentage_key = 'cutoff_percentage'
            if percentage_key in config[cls.config_section]:
                percentage = float(config[cls.config_section][percentage_key])
                if not 0 <= percentage <= 1:
                    raise ValueError('Percentage should be represented with a real number between 0 and 1')
            # Optionally they can provide a gene_scope
            gene_scope = None
            gene_scope_key = 'cutoff_gene_scope'
            if gene_scope_key in config[cls.config_section]:
                gene_scope = int(config[cls.config_section][gene_scope_key])
                if not 1 <= gene_scope <= Player.genes:
                    raise ValueError('Gene scope should be an integer between 1 and ' + str(Player.genes))
            if gene_scope is None:
                if percentage is None:
                    return StructureCutoff(val)
                else:
                    return StructureCutoff(val, population_percentage=percentage)
            else:
                if percentage is None:
                    return StructureCutoff(val, genes_scope=gene_scope)
                else:
                    return StructureCutoff(val, population_percentage=percentage, genes_scope=gene_scope)
        # Otherwise invalid value
        raise ValueError('Invalid cutoff method on ' + cls.config_path)

    @classmethod
    def __get_crossover(cls, config: ConfigParser):
        # This method assumes the config has the info already
        key = config[cls.config_section]['crossover_type']
        # Now we look for the value we want
        if key.lower() == OnePointCrossover.key:
            return OnePointCrossover()
        elif key.lower() == TwoPointCrossover.key:
            return TwoPointCrossover()
        elif key.lower() == AnnularCrossover.key:
            return AnnularCrossover()
        elif key.lower() == UniformCrossover.key:
            uniform_crossover_probability_key = 'uniform_crossover_probability'
            if uniform_crossover_probability_key in config[cls.config_section]:
                uniform_crossover_probability = float(config[cls.config_section][uniform_crossover_probability_key])
                if not 0 <= uniform_crossover_probability <= 1:
                    raise ValueError('Probability must be real number between 0 and 1')
                return UniformCrossover(uniform_crossover_probability)
            return UniformCrossover()
        # Otherwise invalid value
        raise ValueError('Invalid crossover value on ' + cls.config_path)

    @classmethod
    def __get_mutation(cls, config: ConfigParser, dataset: Dataset):
        # This method assumes the config has the info already
        key = config[cls.config_section]['mutation_type']
        val = float(config[cls.config_section]['mutation_probability'])
        if not 0 <= val <= 1:
            raise ValueError('Mutation probability must be a real number between 0 and 1.')
        # Now we look for the value we want
        if key.lower() == GenMutation.key:
            return GenMutation(dataset, val)
        elif key.lower() == CompleteMutation.key:
            return CompleteMutation(dataset, val)
        elif key.lower() == LimitedMultigenMutation.key:
            return LimitedMultigenMutation(dataset, val)
        elif key.lower() == UniformMultigenMutation.key:
            return UniformMultigenMutation(dataset, val)
        # Otherwise invalid value
        raise ValueError('Invalid implementation value on ' + cls.config_path)

    @classmethod
    def __get_selection(cls, config: ConfigParser, number: int):
        # Selection goes from 1 to 4
        if not 1 <= number < 5:
            raise ValueError('Invalid parameter: ' + str(number) + ' on __get_selection')
        # This method assumes the config has the info already
        key = config[cls.config_section]['selection_method_' + str(number)]
        # Now we look for the value we want
        if key.lower() == BoltzmannSelection.key:
            # Boltzmann can take a number of optional parameters, for simplicity's sake we will accept all or none
            initial_temperature_key = 'boltzmann_initial_temperature'
            min_temperature_key = 'boltzmann_min_temperature'
            k_key = 'boltzmann_k'
            if initial_temperature_key in config[cls.config_section] and min_temperature_key in config[cls.config_section] and k_key in config[cls.config_section]:
                initial_temperature = float(config[cls.config_section][initial_temperature_key])
                min_temperature = float(config[cls.config_section][min_temperature_key])
                if initial_temperature < min_temperature:
                    raise ValueError('Initial temperature cant be lower tan min temperature')
                k = float(config[cls.config_section][k_key])
                if k < 0:
                    raise ValueError('K cannot be negative, it would make temperature rise instead of decrease')
                return BoltzmannSelection(initial_temperature, min_temperature, k)
            return BoltzmannSelection()
        elif key.lower() == DeterministicTournamentSelection.key:
            # Optionally this selection method takes tournament size
            size_key = 'tournament_size'
            if size_key in config[cls.config_section]:
                size = int(config[cls.config_section][size_key])
                if not size >= 2:
                    raise ValueError('Tournament size must be at least 2')
                return DeterministicTournamentSelection(size)
            return DeterministicTournamentSelection()
        elif key.lower() == EliteSelection.key:
            return EliteSelection()
        elif key.lower() == ProbabilisticTournamentSelection.key:
            # Optionally this selection methods takes a threshold
            threshold_key = 'tournament_threshold'
            if threshold_key in config[cls.config_section]:
                threshold = float(config[cls.config_section][threshold_key])
                if not 0.5 <= threshold <= 1:
                    raise ValueError('Threshold must be a real number between 0.5 and 1')
                return ProbabilisticTournamentSelection(threshold)
            return ProbabilisticTournamentSelection()
        elif key.lower() == RankingSelection.key:
            return RankingSelection()
        elif key.lower() == RouletteSelection.key:
            return RouletteSelection()
        elif key.lower() == UniversalSelection.key:
            return UniversalSelection()
        # Otherwise invalid value
        raise ValueError('Invalid selection method on ' + cls.config_path)
