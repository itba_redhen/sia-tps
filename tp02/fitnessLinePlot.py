import matplotlib.pyplot as plt
import matplotlib.animation as animation
import pandas

# Line plot with values for min, 1st quartile, mean, median, 3rd quartile and max
# figure for fitness
fitness_fig = plt.figure(figsize=(13, 7))
# Interval of update (milliseconds)
fitness_interval = 1000


# Main function that calls the graph
def main():
    # Assignation to prevent garbage collection
    fitness_ani = _fitness_graph()
    # diversity_ani = _diversity_graph()
    plt.show()


def _fitness_graph():
    return animation.FuncAnimation(fitness_fig, _fitness_animate, interval=fitness_interval)


def _fitness_animate(i):
    # Clear previous input
    fitness_fig.clf()
    fitness_ax = fitness_fig.add_subplot(1, 1, 1)
    # Get a dict with the raw data
    raw_data = _get_data()
    # Only show graph if there are enough rows
    raw_data_len = len(raw_data)
    if raw_data_len > 0:
        # Marker shape
        if raw_data_len < 80:
            marker = 'o-'
        else:
            marker = '-'
        # Building Plot
        x = raw_data['gen']
        for col in raw_data:
            if col != 'gen':
                y = raw_data[col]
                fitness_ax.plot(x, y, marker, fillstyle='none', label=col)
        # Title
        fitness_ax.set_title('Fitness per generation')

        # Setting axis labels & ticks
        # adding horizontal grid lines
        fitness_ax.yaxis.grid(True)
        # Axis labels
        fitness_ax.set_xlabel('Generation')
        fitness_ax.set_ylabel('Fitness')

        # Limits
        fitness_ax.set_xlim(xmin=0)

        # Legends, anchored to upper left
        plt.legend(bbox_to_anchor=(1, 1), loc="upper left")


# Returns a dict of all players
def _get_data():
    path = "fitness.tsv"
    try:
        data = pandas.read_csv(path, delimiter='\t')
    except FileNotFoundError:
        return pandas.DataFrame()
    return data


# Main function, wrapper for main()
if __name__ == "__main__":
    main()
