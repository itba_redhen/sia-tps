from abc import ABCMeta, abstractmethod

from game import GameBoard
from solver.nodes import Node


# Parent class for solvers
class Solver:
    # Shared attributes
    def __init__(self, heuristics=None):
        # Solution should be None or an instance of Node with .score it's depth and the trail of .parents
        self.solution = None
        self.expanded_nodes = 0
        self.frontier_final_len = 0
        self.processing_time = 0
        self.heuristics = heuristics

    # Solve function, should return either None for a non solution or a solved State
    def solve(self, game: GameBoard):
        return self.solution

    # Reset previous solution stats
    def reset_solution(self):
        self.solution = None
        self.expanded_nodes = 0
        self.frontier_final_len = 0
        self.processing_time = 0

    # Prints solution data
    def print_solution(self, game: GameBoard):
        if self.solution is None or not isinstance(self.solution, Node):
            print("No solution has been found.")
        else:
            print("Solution has been found.")
            print("Solution depth and cost (uniform cost problem so it's the same):", self.solution.score)
            print("Expanded nodes total:", self.expanded_nodes)
            print("Frontier nodes at the end:", self.frontier_final_len)
            print("Processing time:", self.processing_time, "seconds")
            # Solution steps
            print("Solution states from final to initial:")
            current_node = self.solution
            while current_node is not None:
                print(game.board_as_str(current_node.state))
                current_node = current_node.parent
