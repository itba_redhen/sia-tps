from queue import PriorityQueue
import time

from game import GameBoard
from solver import Solver
from solver.nodes import HeuristicNode
from solver.heuristics import *


class GGSSolver(Solver):
    STRATEGY_ASCII = "ggs"

    def __init__(self, heuristics: Heuristic = None):
        super().__init__(heuristics)
        if heuristics is None:
            self.heuristics = NoHeuristic()
        else:
            self.heuristics = heuristics

    def solve(self, game: GameBoard):
        # Reset class state
        self.reset_solution()
        # Start timer
        start_time = time.time()
        # Creating variables to use in algorithm
        solution = None
        h = self.heuristics
        frontier_len = 0
        # Creating queue and dictionary for keeping track of frontier and explored nodes
        queue = PriorityQueue()
        explored_nodes = dict()
        # Initializing
        root_node = HeuristicNode(game.initial_state, 0, None, h.compute(game, game.initial_state))
        queue.put(root_node)
        frontier_len += 1
        explored_nodes[root_node] = root_node.score
        # Search loop
        while solution is None and not queue.empty():
            # We get next node from priority queue, should be the one with least heuristic score
            curr_node = queue.get()
            frontier_len -= 1

            # If node is solution, we save it and end
            if game.is_cleared(curr_node.state):
                solution = curr_node
            # Otherwise we expand it
            else:
                # First we mark it as expanded
                self.expanded_nodes += 1
                # Then we look at possible actions
                for action in game.get_valid_moves(curr_node.state):
                    # We enact them
                    new_state = game.move_agent(curr_node.state, action)
                    # We create a node with this new state
                    new_node = HeuristicNode(new_state, curr_node.score + 1, curr_node, h.compute(game, new_state))
                    # If it's either a new state or an old state but with better score
                    if new_node not in explored_nodes or explored_nodes[new_node] > new_node.score:
                        # Then we add it to the queue
                        queue.put(new_node)
                        frontier_len += 1
                        # And mark it as explored
                        explored_nodes[new_node] = new_node.score

        # We call time
        self.processing_time = time.time() - start_time
        # We save frontier len
        self.frontier_final_len = frontier_len
        # We save the solution
        self.solution = solution
        # We return the solved state
        if solution is not None:
            return solution.state
        return None

    def print_solution(self, game: GameBoard):
        print("Strategy:", self.STRATEGY_ASCII)
        print("Heuristic:", self.heuristics)
        super().print_solution(game)