from solver import BDFSSolver


class BFSSolver(BDFSSolver):
    STRATEGY_ASCII = "bfs"

    def __init__(self, heuristics=None):
        super().__init__(heuristics)

        self.strategy = self.STRATEGY_ASCII
        self.pop = self.queue.popleft
