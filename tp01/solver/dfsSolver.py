from solver import BDFSSolver


class DFSSolver(BDFSSolver):
    STRATEGY_ASCII = "dfs"

    def __init__(self, heuristics=None):
        super().__init__(heuristics)

        self.strategy = self.STRATEGY_ASCII
        self.pop = self.queue.pop
