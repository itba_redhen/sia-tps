from collections import deque
from solver import Solver
from game import GameBoard
from solver.nodes import Node
import time


class BDFSSolver(Solver):

    def __init__(self, heuristics=None):
        super().__init__(heuristics)
        # Implementation specific attributes
        self.state_map = None
        self.queue = deque()
        self.strategy = None
        self.pop = None

    def solve(self, game: GameBoard):
        # Reset class state
        self.reset_solution()
        # Start timer
        start_time = time.time()
        # setting as local variable
        initial_state = game.initial_state
        # Preparing initial values
        queue = self.queue
        state_map = dict()
        state_map[initial_state] = Node(initial_state, 0, None)
        expanded_nodes = 0

        # We check if it's solved
        if game.is_cleared(initial_state):
            solution = state_map[initial_state]
        else:
            solution = None
        # We get next states
        current_state = initial_state
        expanded_nodes += 1
        for action in game.get_valid_moves(current_state):
            queue.append({"state": current_state, "action": action})

        # Loop searching for solution
        while solution is None and len(queue) > 0:
            # Fetching node from Frontier
            queue_node = self.pop()
            current_state = queue_node["state"]
            current_action = queue_node["action"]

            # We enact the action specified in queue node
            next_state = game.move_agent(current_state, current_action)

            # Evaluating if the node is repeated
            if next_state not in state_map:
                # Get next state
                state_map[next_state] = Node(next_state, state_map[current_state].score+1, state_map[current_state])

                if game.is_cleared(next_state):
                    # Break loop condition
                    solution = state_map[next_state]
                else:
                    expanded_nodes += 1
                    for action in game.get_valid_moves(next_state):
                        # Input successors as frontier nodes
                        queue.append({"state": next_state, "action": action})

        # Found solution, stop timer
        self.processing_time = time.time() - start_time
        # Save solution node
        self.solution = solution
        # We save amount of expanded nodes
        self.expanded_nodes = expanded_nodes
        # We save frontier length
        self.frontier_final_len = len(queue)
        # We save implementation specific data
        self.state_map = state_map
        self.queue = queue
        if isinstance(solution, Node):
            return solution.state
        return None

    def reset_solution(self):
        super().reset_solution()
        self.queue.clear()
        del self.state_map
        self.state_map = None

    def print_solution(self, game: GameBoard):
        print("Strategy:", self.strategy)
        super().print_solution(game)
