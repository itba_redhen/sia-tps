from queue import PriorityQueue
import time

from game import GameBoard
from solver import Solver
from solver.nodes import AStarNode
from solver.heuristics import *


class ASSolver(Solver):
    STRATEGY_ASCII = "as"

    def __init__(self, heuristics: Heuristic = NoHeuristic()):
        super().__init__(heuristics)

    def solve(self, game: GameBoard):
        # Reset class state
        self.reset_solution()
        # Start timer
        start_time = time.time()

        explored_nodes = dict()
        expanded_nodes = 0
        total_nodes = 0
        self_heuristic = self.heuristics
        initial_state = game.initial_state
        queue = PriorityQueue()
        solution = None

        current_heuristic = self_heuristic.compute(game, initial_state)
        current_node = AStarNode(initial_state, 0, None, current_heuristic)
        queue.put(current_node)
        total_nodes += 1

        while solution is None and not queue.empty():
            current_node = queue.get()
            current_state = current_node.state

            if game.is_cleared(current_state):
                solution = current_node
            elif current_state not in explored_nodes or current_node.score < explored_nodes[current_state].score:
                # We going to expand the node, first we mark it on the status
                expanded_nodes += 1
                # Then we look at the possible actions
                for action in game.get_valid_moves(current_state):
                    total_nodes += 1
                    # Enact them
                    new_state = game.move_agent(current_state, action)
                    # Compute heuristic
                    if new_state in explored_nodes:
                        new_state_heuristic_score = explored_nodes[new_state].heuristic_score
                    else:
                        new_state_heuristic_score = self_heuristic.compute(game, new_state)

                    # Create a node with this new state & heuristic
                    new_node = AStarNode(new_state, current_node.score + 1, current_node,
                                         new_state_heuristic_score)

                    if new_state not in explored_nodes or new_node.score < explored_nodes[new_state].score:
                        # Only when we have a better score it is worth adding to the queue
                        queue.put(new_node)

                # Overwrite explored_nodes value only if the score is better
                explored_nodes[current_state] = current_node

        # Found solution, stop timer
        self.processing_time = time.time() - start_time
        # Save solution node
        self.solution = solution
        # We save amount of expanded nodes
        self.expanded_nodes = expanded_nodes
        # We save frontier length
        self.frontier_final_len = queue.qsize()

        if solution is not None:
            return solution.state
        return None

    def print_solution(self, game: GameBoard):
        print("Strategy:\t", self.STRATEGY_ASCII)
        print("Heuristic:\t", self.heuristics)
        super().print_solution(game)
