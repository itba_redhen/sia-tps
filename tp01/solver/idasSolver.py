import math
import time
from collections import deque
from queue import PriorityQueue

from game import GameBoard
from solver import Solver
from solver.heuristics import *
from solver.nodes import AStarNode


class IDASSolver(Solver):
    STRATEGY_ASCII = "idas"

    def __init__(self, heuristics: Heuristic = NoHeuristic()):
        super().__init__(heuristics)

    def solve(self, game: GameBoard):
        # Reset class state
        self.reset_solution()
        # Start timer
        start_time = time.time()
        # Initial depth is f(n0) and since g(n0) is 0 we just do h(n0)
        current_depth = self.heuristics.compute(game, game.initial_state)
        # Setting up variables for use in loop
        solution = None
        final_frontier = set()

        # We look for solution changing depth
        while solution is None:
            # We look for a new solution
            solution = self._find_solution(game, current_depth, final_frontier)

            # We see how to change depth based on previous runs
            # If we have no solution
            if solution is None:
                # Aux variable initialization
                costs = set()
                # Then we look at the nodes in the queue (there should only be nodes that exceeded previous limit)
                for node in final_frontier:
                    # We add it's cost f(n) to the set
                    costs.add(node.score + node.heuristic_score)
                # Now we get the minimum cost from the set
                current_depth = min(costs)
                # And we clear the set for next iteration
                final_frontier.clear()

        # We call time
        self.processing_time = time.time() - start_time
        # At this point we have the optimal solution, so we save it
        self.solution = solution
        # And we return the solved state
        if isinstance(solution, AStarNode):
            return solution.state
        return None

    def _find_solution(self, game: GameBoard, current_max_depth, final_frontier):
        # Creating variables to use in the algorithm
        solution = None
        self_heuristic = self.heuristics
        # Creating initial tree node to be modified by find_solution()
        root_node = AStarNode(game.initial_state, 0, None, self_heuristic.compute(game, game.initial_state))
        # Creating queue to hold frontier and explored nodes map
        queue = deque()
        explored_nodes = dict()
        # Starting frontier with the first node
        queue.append(root_node)
        self.frontier_final_len = 1
        explored_nodes[root_node] = root_node.score

        # Looking for solution at current max_depth
        while solution is None and len(queue) > 0:
            # We get the next node from frontier (queue.pop since it's dfs)
            current_node = queue.pop()
            self.frontier_final_len -= 1

            # If node is solution, we save it and end
            if game.is_cleared(current_node.state):
                solution = current_node
            # Otherwise, we expand it
            else:
                # We going to expand the node, first we mark it on the status
                self.expanded_nodes += 1
                # Then we look at the possible actions
                for action in game.get_valid_moves(current_node.state):
                    # Enact them
                    new_state = game.move_agent(current_node.state, action)
                    # Compute heuristic
                    if new_state in explored_nodes:
                        new_state_heuristic_score = explored_nodes[new_state].heuristic_score
                    else:
                        new_state_heuristic_score = self_heuristic.compute(game, new_state)
                    # Create a node with this new state & heuristic
                    new_node = AStarNode(new_state, current_node.score + 1, current_node,
                                         new_state_heuristic_score)
                    # If it's either a new state or an old state but with better score
                    if new_node not in explored_nodes or explored_nodes[new_node] > new_node.score:
                        # And new node cost is within limits
                        if new_node.score + new_state_heuristic_score <= current_max_depth:
                            # Then we add it to queue
                            queue.append(new_node)
                            self.frontier_final_len += 1
                            explored_nodes[new_node] = new_node.score
                    # If node was above the depth limit then we add it to the final frontier for next depth
                    if new_node.score + new_node.heuristic_score > current_max_depth:
                        final_frontier.add(new_node)

        # Once we finish, we either found a solution or we ran out of nodes in frontier
        if solution is not None:
            return solution
        else:
            return None

    def print_solution(self, game: GameBoard):
        print("Strategy:\t", self.STRATEGY_ASCII)
        print("Heuristic:\t", self.heuristics)
        super().print_solution(game)
