from game.helpers import State
from solver.nodes import Node


# A state always has always the same heuristic score therefore overwriting of __eq__ & __hash_ not needed
class HeuristicNode(Node):
    def __init__(self, node_state: State, node_score, node_parent, heuristic_score):
        super().__init__(node_state, node_score, node_parent)
        self.heuristic_score = heuristic_score

    def __eq__(self, other):
        return isinstance(other, HeuristicNode) and super().__eq__(other)

    def __lt__(self, other):
        comparison = self.heuristic_score - other.heuristic_score
        if comparison == 0:
            return self.score < other.score
        else:
            return comparison < 0

    def __hash__(self):
        return hash(self.state)