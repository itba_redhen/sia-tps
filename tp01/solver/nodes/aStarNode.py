from game.helpers import State
from solver.nodes import HeuristicNode


# A state always has always the same heuristic score therefore overwriting of __eq__ & __hash_ not needed
class AStarNode(HeuristicNode):
    def __init__(self, node_state: State, node_score, node_parent, heuristic_score):
        super().__init__(node_state, node_score, node_parent, heuristic_score)

    def __lt__(self, other):
        comparison = self.score + self.heuristic_score - (other.score + other.heuristic_score)
        if comparison == 0:
            return self.heuristic_score < other.heuristic_score
        else:
            return comparison < 0
