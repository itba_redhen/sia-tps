from game.helpers import State


class Node:
    def __init__(self, node_state: State, node_score, node_parent):
        self.state = node_state
        self.score = node_score
        self.parent = node_parent

    def __eq__(self, other):
        if not isinstance(other, Node):
            return False
        return self.state == other.state

    def __hash__(self):
        return hash(self.state)