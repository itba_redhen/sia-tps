import math
import time
from collections import deque

from game import GameBoard
from solver import Solver
from solver.nodes import Node


class IDDFSSolver(Solver):
    STRATEGY_ASCII = "iddfs"
    DEFAULT_DEPTH = 10
    DEFAULT_STEPS = 5

    def __init__(self, optimal: bool, heuristics=None, initial_max_depth=DEFAULT_DEPTH, steps=DEFAULT_STEPS):
        super().__init__(heuristics)

        self.optimal = optimal
        self.initial_max_depth = int(initial_max_depth)
        self.steps = int(steps)

    def solve(self, game: GameBoard):
        # Reset class state
        self.reset_solution()
        # Start timer
        start_time = time.time()
        # Initial depth
        current_depth = self.initial_max_depth
        # Setting up variables for use in loop
        current_solution = None
        highest_failed_depth = 0

        # We look for solution changing depth
        while True:
            # We look for a new solution
            new_solution = self._find_solution(game, current_depth)

            # If we are not looking for optimal solution
            if not self.optimal and new_solution is not None:
                # We save it and finish
                current_solution = new_solution
                break

            # We see how to change depth based on previous runs
            # If we have no solution
            if current_solution is None:
                # And we have not found a new solution
                if new_solution is None:
                    # if it's the new highest fail we save it
                    if current_depth > highest_failed_depth:
                        highest_failed_depth = current_depth
                    # then we increase depth to try to find at least one
                    next_depth = current_depth + self.steps
                # And we found a new solution
                else:
                    # We save it
                    current_solution = new_solution
                    # And try with the midpoint between highest failed and current depth
                    next_depth = math.floor((current_solution.score + highest_failed_depth) / 2)
            # If we have a previous solution
            else:
                # And we have not found a new solution
                if new_solution is None:
                    # if it's the new highest fail we save it
                    if current_depth > highest_failed_depth:
                        highest_failed_depth = current_depth
                    # then we try to find one between the current depth we just tried and the depth of known solution
                    next_depth = math.floor((current_solution.score + current_depth) / 2)
                # And we have found a new solution
                else:
                    # We save this solution
                    current_solution = new_solution
                    # And we try with midpoint between highest failed and current depth
                    next_depth = math.floor((current_solution.score + highest_failed_depth) / 2)

            # If the next_depth is equal to the depth we just tried, then we stop
            if next_depth == current_depth:
                break
            # Otherwise we go to next depth
            current_depth = next_depth

        # We call time
        self.processing_time = time.time() - start_time
        # At this point we have the optimal solution, so we save it
        self.solution = current_solution
        # And we return the solved state
        if isinstance(current_solution, Node):
            return current_solution.state
        return None

    def _find_solution(self, game: GameBoard, current_max_depth):
        # Creating variables to use in the algorithm
        solution = None
        # Creating initial tree node to be modified by find_solution()
        root_node = Node(game.initial_state, 0, None)
        # Creating queue to hold frontier and explored nodes map
        queue = deque()
        explored_nodes = dict()
        # Starting frontier with the first node
        queue.append(root_node)
        self.frontier_final_len = 1
        explored_nodes[root_node] = root_node.score

        # Looking for solution at current max_depth
        while solution is None and len(queue) > 0:
            # We get the next node from frontier (queue.pop since it's dfs)
            current_node = queue.pop()
            self.frontier_final_len -= 1

            # If node is solution, we save it and end
            if game.is_cleared(current_node.state):
                solution = current_node
            # Otherwise, we expand it
            else:
                # We going to expand the node, first we mark it on the status
                self.expanded_nodes += 1
                # Then we look at the possible actions
                for action in game.get_valid_moves(current_node.state):
                    # Enact them
                    new_state = game.move_agent(current_node.state, action)
                    # Create a node with this new state
                    new_node = Node(new_state, current_node.score + 1, current_node)
                    # If it's either a new state or an old state but with better score
                    if new_node not in explored_nodes or explored_nodes[new_node] > new_node.score:
                        # And new node score is within limits
                        if new_node.score <= current_max_depth:
                            # Then we add it to queue
                            queue.append(new_node)
                            self.frontier_final_len += 1
                            explored_nodes[new_node] = new_node.score

        # Once we finish, we either found a solution or we ran out of nodes in frontier
        if solution is not None:
            return solution
        else:
            return None

    def print_solution(self, game: GameBoard):
        print("Strategy:", self.STRATEGY_ASCII)
        print("Solution is optimal:", self.optimal)
        super().print_solution(game)
