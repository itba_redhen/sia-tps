from solver.heuristics import *
from game import GameBoard
from game.helpers import State


class HeuristicCollection(Heuristic):
    HEURISTIC_STRATEGY = "Heuristic Collection"
    HEURISTIC_ASCII = "hc"

    def __init__(self, *heuristics):
        self.heuristics = list(filter(lambda h: isinstance(h, Heuristic), heuristics))
        if len(self.heuristics) == 0:
            self.heuristics.append(NoHeuristic())

    def compute(self, game: GameBoard, state: State):
        return max(map(lambda h: h.compute(game, state), self.heuristics))

    def __len__(self):
        return len(self.heuristics)

    def __repr__(self):
        st = self.HEURISTIC_STRATEGY + ":{"
        st += ''.join(map(lambda h: repr(h) + ", ", self.heuristics))
        st = st[:-2]
        st += "}"

        return st
