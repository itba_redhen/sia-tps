from game import GameBoard
from game.helpers import State, Box
from solver.heuristics import Heuristic
from munkres import Munkres
import math


class HungarianHeuristic(Heuristic):
    HEURISTIC_STRATEGY = "Hungarian Algorithm"
    HEURISTIC_ASCII = "hh"

    # Heuristic creates matrix of goal, boxes and their associated cost.
    # Cost is the manhattan distance from box to goal
    # Best combination of box, goal pairs is decided by hungarian algorithm
    # As an addendum we decided to punish moves that get further away from boxes
    # It's O(n^3) where n is the number of goals or number of boxes, should be same
    def compute(self, game: GameBoard, state: State):
        # if game cleared we return 0
        if game.is_cleared(state):
            return 0
        # if game is deadlocked we return inf
        if game.is_blocked(state):
            return math.inf
        # We get boxes
        boxes = [k for (k, v) in state.environment.items() if isinstance(v, Box)]
        # We get goals
        goals = game.static_environment.goal_position
        # We create the matrix
        matrix = list()
        # We load the matrix with the data
        i = 0
        for b in boxes:
            box_row = list()
            j = 0
            for g in goals:
                # We calculate the manhattan distance from box to goal
                cost = (g - b).get_distance()
                # We add it to the box row
                box_row.append(cost)
                j += 1
            # We add the box row to the matrix and go to the next box
            matrix.append(box_row)
            i += 1
        # We call the hungarian algorithm to match boxes with goals in a way that we get the lowest total cost
        m = Munkres()
        indexes = m.compute(matrix)
        # We determine the total cost, which should be the minimum cost for this scenario
        total = 0
        for row, column in indexes:
            value = matrix[row][column]
            total += value
        # Now we check how close is the player to any of the boxes
        agent = state.agent.position
        agent_min_distance_to_box = min(map(lambda box: (box - agent).get_distance(), boxes))
        # And we return the sum
        return total + agent_min_distance_to_box - 1
