import math

from solver.heuristics import Heuristic
from game import GameBoard
from game.helpers import *


class AveragedDistanceAdmissibleHeuristic(Heuristic):

    HEURISTIC_STRATEGY = "Averaged Distance Admissible"
    HEURISTIC_ASCII = "ada"

    # This heuristic returns the sum of the combined multiplication of the distance between each box with each goal
    # It is then made admissible by multiplying it to a value V that guarantees that is lower than h*(n)
    # It uses V following the next mathematical rule:
    # V=(a/c), V*b < a, where a <= b <= c
    # O(b*g) time complexity, where b is the number of boxes, and g the number of goals
    def compute(self, game: GameBoard, state: State):

        if game.is_cleared(state):
            return 0

        boxes = [k for (k, v) in state.environment.items() if isinstance(v, Box)]
        goals = game.static_environment.goal_position

        # Exponent of the root
        goals_exponent = 1/len(goals)

        min_possible_distance = 0
        max_possible_distance = 0
        box_avg_distance_to_goal = 0
        for b in boxes:
            box_distance_to_goals = list(map(lambda p: (p-b).get_distance(), goals))

            min_possible_distance += min(box_distance_to_goals)
            max_possible_distance += max(box_distance_to_goals)
            box_avg_distance_to_goal += math.pow(math.prod(box_distance_to_goals), goals_exponent)

        return (min_possible_distance/max_possible_distance)*box_avg_distance_to_goal
