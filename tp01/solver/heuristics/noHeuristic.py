from solver.heuristics import Heuristic
from game import GameBoard
from game.helpers import *


class NoHeuristic(Heuristic):

    HEURISTIC_STRATEGY = "No heuristic"
    HEURISTIC_ASCII = "no"

    # This heuristic returns 0 if cleared, 
    # 1 otherwise (and act like an uniformed search)
    # Created as default where no heuristic is specified
    # O(g) time complexity where g is the number of goals
    def compute(self, game: GameBoard, state: State):
        return 0 if (game.is_cleared(state)) else 1
