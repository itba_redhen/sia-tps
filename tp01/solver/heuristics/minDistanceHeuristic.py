import math
from solver.heuristics import Heuristic
from game import GameBoard
from game.helpers import *


class MinDistanceHeuristic(Heuristic):
    HEURISTIC_STRATEGY = "Simplified Min Distance"
    HEURISTIC_ASCII = "md"

    # This heuristic sums the manhattan distance between each block and its closest goal.
    # O(b*g) time complexity, where b is the number of boxes, and g the number of goals
    def compute(self, game: GameBoard, state: State):

        if game.is_cleared(state):
            return 0

        boxes = [k for (k, v) in state.environment.items() if isinstance(v, Box)]
        goals = game.static_environment.goal_position

        box_min_distance_to_goal = 0
        for b in boxes:
            b_min_distance_to_goal = math.inf
            for g in goals:
                b_min_distance_to_goal = min(b_min_distance_to_goal, (g - b).get_distance())
            box_min_distance_to_goal += b_min_distance_to_goal

        return box_min_distance_to_goal
