import math
from solver.heuristics import Heuristic
from game import GameBoard
from game.helpers import *


class PruneDeadStatesHeuristic(Heuristic):

    HEURISTIC_STRATEGY = "Dead State Pruning"
    HEURISTIC_ASCII = "pds"

    # This heuristic returns inf for dead states,
    # 0 if cleared, 1 otherwise (and act like an uniformed search)
    # O(n) time complexity
    def compute(self, game: GameBoard, state: State):

        if game.is_cleared(state):
            return 0
        
        if game.is_blocked(state):
            return math.inf
        
        return 1
