import math
from solver.heuristics import Heuristic
from game import GameBoard
from game.helpers import *


class MinDistanceWithAgentHeuristic(Heuristic):
    HEURISTIC_STRATEGY = "Simplified Min Distance with Agent distance to Box"
    HEURISTIC_ASCII = "mda"

    # A slight modification from the minDistanceHeuristic.
    # This heuristic sums the distance between each block and its closest goal and
    # the distance between the agent and its closest goal -1.
    # Rewards the agent when getting closer to boxes and boxes closer to goals
    # O(b*g) time complexity, where b is the number of boxes, and g the number of goals
    def compute(self, game: GameBoard, state: State):

        boxes = [k for (k, v) in state.environment.items() if isinstance(v, Box)]
        goals = game.static_environment.goal_position
        agent = state.agent.position

        # since the game is not cleared, then at least 1 box isn't on goal,
        # therefore this value won't be inf
        agent_min_distance_to_box = min(map(lambda box: math.inf if (box in goals) else (box - agent).get_distance(),
                                            boxes))

        # if agent_min_distance_to_box then all goals are filled by boxes
        if math.isinf(agent_min_distance_to_box):
            return 0

        box_min_distance_to_goal = 0
        for b in boxes:
            b_min_distance_to_goal = math.inf
            for g in goals:
                b_min_distance_to_goal = min(b_min_distance_to_goal, (g - b).get_distance())
            box_min_distance_to_goal += b_min_distance_to_goal

        return box_min_distance_to_goal + agent_min_distance_to_box - 1

