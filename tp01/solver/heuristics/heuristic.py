from abc import ABCMeta, abstractmethod

from game import GameBoard
from game.helpers import State


# Interface for heuristics, must implement compute method
class Heuristic(metaclass=ABCMeta):
    HEURISTIC_STRATEGY = "Undefined Strategy"
    HEURISTIC_ASCII = "Undefined"

    @abstractmethod
    def compute(self, game: GameBoard, state: State):
        pass

    def __repr__(self):
        return self.HEURISTIC_STRATEGY
