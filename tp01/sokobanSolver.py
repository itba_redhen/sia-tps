import getopt
import sys

from game import *
from solver import *
from solver.heuristics import *

# Short option flags
HELP_OPTION_SHORT = "h"
FILE_OPTION_SHORT = "f"
METHOD_OPTION_SHORT = "m"
DEPTH_OPTION_SHORT = "d"
STEP_OPTION_SHORT = "s"
OPTIMAL_OPTION_SHORT = "o"
HEURISTIC_OPTION_SHORT = "e"
# Long option flags
HELP_OPTION_LONG = "help"
FILE_OPTION_LONG = "file"
METHOD_OPTION_LONG = "search-method"
DEPTH_OPTION_LONG = "depth"
STEP_OPTION_LONG = "step"
OPTIMAL_OPTION_LONG = "optimal"
HEURISTIC_OPTION_LONG = "heuristic"
# Option arrays
SHORT_OPTIONS = HELP_OPTION_SHORT + FILE_OPTION_SHORT + ":" + METHOD_OPTION_SHORT + ":" + DEPTH_OPTION_SHORT + ":" \
                + STEP_OPTION_SHORT + ":" + OPTIMAL_OPTION_SHORT + HEURISTIC_OPTION_SHORT + ":"
LONG_OPTIONS = [HELP_OPTION_LONG, FILE_OPTION_LONG + "=", METHOD_OPTION_LONG + "=",
                DEPTH_OPTION_LONG + "=", STEP_OPTION_LONG + "=", OPTIMAL_OPTION_LONG,
                HEURISTIC_OPTION_LONG + "="]
# Supported values
SUPPORTED_SEARCH_METHODS = [BFSSolver.STRATEGY_ASCII, DFSSolver.STRATEGY_ASCII, IDDFSSolver.STRATEGY_ASCII,
                            GGSSolver.STRATEGY_ASCII, ASSolver.STRATEGY_ASCII, IDASSolver.STRATEGY_ASCII]

# Supported heuristics
SUPPORTED_HEURISTICS = [MinDistanceHeuristic.HEURISTIC_ASCII, MinDistanceWithAgentHeuristic.HEURISTIC_ASCII,
                        HungarianHeuristic.HEURISTIC_ASCII, PruneDeadStatesHeuristic.HEURISTIC_ASCII,
                        AveragedDistanceAdmissibleHeuristic.HEURISTIC_ASCII, NoHeuristic.HEURISTIC_ASCII]


def main(argv):
    # Retrieving options
    map_file_path = None
    search_method = None
    iddfs_depth = IDDFSSolver.DEFAULT_DEPTH
    iddfs_step = IDDFSSolver.DEFAULT_STEPS
    iddfs_optimal = False
    h_method = set()
    try:
        opts, args = getopt.getopt(argv, SHORT_OPTIONS, LONG_OPTIONS)
    except getopt.GetoptError:
        print_help()
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-" + HELP_OPTION_SHORT, "--" + HELP_OPTION_LONG):
            print_help()
            sys.exit()
        elif opt in ("-" + FILE_OPTION_SHORT, "--" + FILE_OPTION_LONG):
            map_file_path = arg
        elif opt in ("-" + METHOD_OPTION_SHORT, "--" + METHOD_OPTION_LONG):
            search_method = arg
        elif opt in ("-" + DEPTH_OPTION_SHORT, "--" + DEPTH_OPTION_LONG):
            if int(arg) > 0:
                iddfs_depth = int(arg)
        elif opt in ("-" + STEP_OPTION_SHORT, "--" + STEP_OPTION_LONG):
            if int(arg) > 0:
                iddfs_step = int(arg)
        elif opt in ("-" + OPTIMAL_OPTION_SHORT, "--" + OPTIMAL_OPTION_LONG):
            iddfs_optimal = True
        elif opt in ("-" + HEURISTIC_OPTION_SHORT, "--" + HEURISTIC_OPTION_LONG):
            h_method.add(arg.lower())

    # Ensuring mandatory options where provided
    if map_file_path is None:
        print_help()
        sys.exit(2)
    # Defaulting values for not mandatory arguments
    if search_method is None or search_method.lower() not in SUPPORTED_SEARCH_METHODS:
        search_method = SUPPORTED_SEARCH_METHODS[0]
    # Parsing input and extracting ASCII map
    file = open(map_file_path, 'r')
    ascii_map = ''.join(file.readlines())
    file.close()

    # Creating game board from ascii map, checking success before continuing
    try:
        game_board = GameBoard.from_ascii(ascii_map)
    except ValueError as e:
        # Not a valid map, unsolvable
        print(e.args)
        sys.exit(2)

    # We start the asked heuristic
    heuristics = list()

    for h_m in h_method:
        if h_m == MinDistanceHeuristic.HEURISTIC_ASCII:
            heuristics.append(MinDistanceHeuristic())
        elif h_m == MinDistanceWithAgentHeuristic.HEURISTIC_ASCII:
            heuristics.append(MinDistanceWithAgentHeuristic())
        elif h_m == HungarianHeuristic.HEURISTIC_ASCII:
            heuristics.append(HungarianHeuristic())
        elif h_m == PruneDeadStatesHeuristic.HEURISTIC_ASCII:
            heuristics.append(PruneDeadStatesHeuristic())
        elif h_m == AveragedDistanceAdmissibleHeuristic.HEURISTIC_ASCII:
            heuristics.append(AveragedDistanceAdmissibleHeuristic())

    len_heuristics = len(heuristics)
    if len_heuristics == 0:
        current_heuristic = NoHeuristic()
    elif len_heuristics == 1:
        current_heuristic = heuristics[0]
    else:
        current_heuristic = HeuristicCollection(*heuristics)

    # Solve game using the asked search_method
    if search_method == IDDFSSolver.STRATEGY_ASCII:
        current_solver = IDDFSSolver(optimal=iddfs_optimal, initial_max_depth=iddfs_depth, steps=iddfs_step)
    elif search_method == DFSSolver.STRATEGY_ASCII:
        current_solver = DFSSolver()
    elif search_method == ASSolver.STRATEGY_ASCII:
        current_solver = ASSolver(current_heuristic)
    elif search_method == IDASSolver.STRATEGY_ASCII:
        current_solver = IDASSolver(current_heuristic)
    elif search_method == GGSSolver.STRATEGY_ASCII:
        current_solver = GGSSolver(current_heuristic)
    else:
        current_solver = BFSSolver()

    current_solver.solve(game_board)
    # Print solution and rest of data
    current_solver.print_solution(game_board)


def print_help():
    print("For BFS/DFS use following syntax: sokobanSolver.py -" + METHOD_OPTION_SHORT + " <search-method-code> -"
          + FILE_OPTION_SHORT + " <path-to-map-file>")
    print("For GGS/A* use following syntax: sokobanSolver.py -" + METHOD_OPTION_SHORT + " <search-method-code> -"
          + HEURISTIC_OPTION_SHORT + " <heuristic-code> -" + FILE_OPTION_SHORT + " <path-to-map-file>")
    print("For IDDFS/IDA* use following syntax: sokobanSolver.py -" + METHOD_OPTION_SHORT + " <search-method-code> -"
          + HEURISTIC_OPTION_SHORT + " <ida*-heuristic-code> -" + DEPTH_OPTION_SHORT + " <initial-depth> -"
          + STEP_OPTION_SHORT + " <step-size> -" + FILE_OPTION_SHORT + " <path-to-map-file>")
    print("Supported search methods:")
    methods = ''
    for method in SUPPORTED_SEARCH_METHODS:
        methods += method + "/"
    print(methods[:-1])
    print("Supported heuristics:")
    heuristics = ''
    for heu in SUPPORTED_HEURISTICS:
        heuristics += heu + "/"
    print(heuristics[:-1])


if __name__ == "__main__":
    main(sys.argv[1:])
