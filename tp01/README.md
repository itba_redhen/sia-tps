# SIA TP 1 - Métodos de búsqueda Desinformados e Informados

## Dependencias Necesarias
* GNU make
* pip
* [pipenv](https://pipenv.pypa.io/en/latest/)

## Instalación y Uso
### Paso 0: Ir a la carpeta tp01
``
cd tp01
``

### Paso 1: Preparar el entorno
``
make build
``

### (Opcional): Configurar mediante .env
El archivo ``.env`` contiene las siguientes variables de entorno:
* **OUT_DIR:** directorio para los archivos finales
* **MAPS_DIR:** directorio de los mapas a utilizar
* **STRATEGY:** indica la estrategia predeterminada
* **STRATEGIES:** filtra las estrategias a considerar con ``make map``
* **MAP:** indica el mapa predeterminado
* **MAPS:** filtra los mapas a considerar con ``make strategy``
* **ADDITIONAL_FLAGS:** parametros adicionales, como el ``-o`` para métodos Iterative Deepening óptimo
* **DEPTH:** indica la profundidad inicial de los métodos Iterative Deepening
* **STEP:** indica el valor del incremento de la profundidad máxima en cada iteración de los métodos Iterative Deepening
* **HEURISTICS:** indica las heurísticas a utilizar
* **HEURISTICS_BASIC:** indica las heurísticas a combinar en ``make heuristics``.

### Paso 2: Ejecutar el archivo
``
make run
``

Al no pasarle parámetros corre la estrategia y mapa indicado en ``.env``.

Se le puede sobreescribir la estrategia y el mapa de interés de la siguiente manera:

``
make run STRATEGY=bfs MAP=map01.txt
``

## Adicionalmente
### Ver Ayuda
``
make help
``
### Evaluar un mapa con cada una de las heurísticas
``
make heuristics
``
### Evaluar un mapa con cada una de las heurísticas en ggs, as & ias
``
make id
``
### Evaluar todas las estrategias en un mismo mapa
``
make map MAP=map01.txt
``
### Evaluar todos los mapas con una misma estrategia
``
make strategy STRATEGY=bfs
``
### Evaluar todos los mapas con todas las estrategias
``
make all
``
### Borrar todos los archivos generados
``
make clean
``