from dataclasses import dataclass
from game.helpers import Position, Goal


@dataclass
class StaticEnvironment:
    board_size: Position
    environment: dict
    goal_position: list

    def __init__(self, board_size, environment):
        self.board_size = board_size
        self.environment = environment

        self.goal_position = list()
        for maybeGoalPosition in self.environment:
            if isinstance(self.environment[maybeGoalPosition], Goal):
                self.goal_position.append(maybeGoalPosition)
