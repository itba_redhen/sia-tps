from game.helpers import GameObject


class Wall(GameObject):
    STEPPABLE = False
    ASCII_SYMBOL = "#"

    def is_movable(self, pusher):
        return False

    def __repr__(self):
        return self.ASCII_SYMBOL
