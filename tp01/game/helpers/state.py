from dataclasses import dataclass
from game.helpers import Agent


@dataclass
class State:
    agent: Agent
    environment: dict

    def __eq__(self, other):
        if not isinstance(other, State):
            return False
        return hash(self._freeze()) == hash(other._freeze())

    def __hash__(self):
        return hash(self._freeze())

    def _freeze(self):
        return frozenset({self.agent, frozenset(sorted(self.environment.items()))})
