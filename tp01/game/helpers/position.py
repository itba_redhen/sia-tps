from dataclasses import dataclass


@dataclass(frozen=True)
class Position:
    x: int = 0
    y: int = 0

    def __lt__(self, other):
        return abs(self.x) + abs(self.y) < abs(other.x) + abs(other.y)

    def __eq__(self, other):
        return isinstance(other, Position) and self.x == other.x and self.y == other.y

    def __add__(self, other):
        return Position(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return Position(self.x - other.x, self.y - other.y)
    
    def get_distance(self):
        return abs(self.x) + abs(self.y)
