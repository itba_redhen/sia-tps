from game.helpers import GameObject, Agent


class Box(GameObject):
    STEPPABLE = False
    ASCII_SYMBOL = "$"
    ASCII_SYMBOL_GOAL = "*"

    # pusher is player => push, else stop
    def is_movable(self, pusher):
        return isinstance(pusher, Agent)

    def __eq__(self, other):
        return isinstance(other, Box)

    def __hash__(self):
        return hash(type(Box))

    def __repr__(self):
        return self.ASCII_SYMBOL + self.ASCII_SYMBOL_GOAL
