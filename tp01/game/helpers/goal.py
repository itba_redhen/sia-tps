from game.helpers import GameObject


class Goal(GameObject):
    STEPPABLE = True
    ASCII_SYMBOL = "."

    def __repr__(self):
        return self.ASCII_SYMBOL
