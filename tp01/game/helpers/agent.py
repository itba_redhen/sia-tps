from game.helpers import GameObject, Position


class Agent(GameObject):
    ACCEPTED_ACTIONS = {Position(1, 0), Position(-1, 0), Position(0, 1), Position(0, -1)}
    STEPPABLE = False
    ASCII_SYMBOL = "@"
    ASCII_SYMBOL_GOAL = "+"

    def __init__(self, position: Position):
        self.position = position

    def __eq__(self, other):
        return isinstance(other, Agent) and self.position == other.position

    def __hash__(self):
        return hash((type(Agent), self.position))

    def __repr__(self):
        return self.ASCII_SYMBOL + self.ASCII_SYMBOL_GOAL
