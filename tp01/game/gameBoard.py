import copy

from game.helpers import *


class GameBoard:
    ASCII_EMPTY_SPACE = " "
    ASCII_NEW_ROW = "\n"

    def __init__(self, static_environment: StaticEnvironment, initial_state: State):
        self.static_environment = static_environment
        self.initial_state = initial_state

    def board_as_str(self, curr_state):
        # Initialize board string
        board_str = ""
        # Get board size and references to static and dynamic environment
        board_size = self.static_environment.board_size
        static_objects = self.static_environment.environment
        boxes = curr_state.environment
        agent_position = curr_state.agent.position
        # and start iterating over positions
        for i in range(0, board_size.y):
            for j in range(0, board_size.x):
                # We determine current position
                current_position = Position(j, i)
                # We try and find what's in this position
                if current_position in static_objects:
                    static_object = static_objects[current_position]
                else:
                    # GameObject used to represent empty position
                    static_object = GameObject()
                # If static object is a wall, we ignore the rest
                if isinstance(static_object, Wall):
                    board_str += Wall.ASCII_SYMBOL
                else:
                    # If it's goal we use certain symbols, otherwise it's empty space
                    is_goal = isinstance(static_object, Goal)
                    # Now let's see what else could be here, first off boxes
                    if current_position in boxes and isinstance(boxes[current_position], Box):
                        if is_goal:
                            board_str += Box.ASCII_SYMBOL_GOAL
                        else:
                            board_str += Box.ASCII_SYMBOL
                    # If there is no box, then maybe there is an agent
                    elif agent_position.x == current_position.x and agent_position.y == current_position.y:
                        if is_goal:
                            board_str += Agent.ASCII_SYMBOL_GOAL
                        else:
                            board_str += Agent.ASCII_SYMBOL
                    # If there is neither, then it's either a goal or an empty space
                    else:
                        if is_goal:
                            board_str += Goal.ASCII_SYMBOL
                        else:
                            board_str += self.ASCII_EMPTY_SPACE
            # After we finish a row, we add a new line symbol
            board_str += self.ASCII_NEW_ROW

        return board_str

    # Method returns the state after player moves in direction
    def move_agent(self, curr_state, direction):
        # If direction is not supported
        if direction not in curr_state.agent.ACCEPTED_ACTIONS:
            # then no change in the state
            return curr_state
        # We calculate the new position for the agent
        new_position = curr_state.agent.position + direction

        # case 0: Agent is out of bounds
        if self._out_of_bounds(new_position):
            # then no change in the state
            return curr_state

        # case 1: there is a dynamic object (a box cuz new position is calculated from agent position)
        if new_position in curr_state.environment:
            # We check if box is pushable
            if self._is_pushable(curr_state, pusher=curr_state.agent,
                                 pushed=curr_state.environment[new_position],
                                 pushed_position=new_position, direction=direction):
                # Use deep copy as this method should keep previous state intact
                current_state = copy.deepcopy(curr_state)
                # Then we move the dynamic object (which would be a box) knowing it wont fail
                current_state = self._push(current_state, new_position, direction)
                current_state.agent.position = current_state.agent.position + direction
                return current_state
            else:
                # If box is not pushable, it means its up against wall or another box
                # No change on the state
                return curr_state
        # case 2: there is a static object, which could be a wall or a goal
        elif new_position in self.static_environment.environment:
            # case 2: there is static object, by definition, these aren't pushable but could be steppable
            obj = self.static_environment.environment[new_position]
            if obj.STEPPABLE:
                # If steppable then we update state
                # Use deep copy as this method should keep previous state intact
                current_state = copy.deepcopy(curr_state)
                current_state.agent.position = current_state.agent.position + direction
                return current_state
            else:
                # No change to the state
                return curr_state
        # case 3: there is nothing, aka floor
        # Use deep copy as this method should keep previous state intact
        current_state = copy.deepcopy(curr_state)
        # We move the player
        current_state.agent.position = current_state.agent.position + direction
        return current_state

    # Returns if there are less movable boxes than total goals
    # If this function returns true then this level is unsolvable
    def is_blocked(self, curr_state: State):

        current_state = curr_state

        goals = len(self.static_environment.goal_position)
        not_blocked_boxes = 0

        for maybe_box_pos in current_state.environment:
            maybe_box = current_state.environment[maybe_box_pos]
            if isinstance(maybe_box, Box):

                if maybe_box_pos in self.static_environment.goal_position:
                    not_blocked_boxes += 1
                else:
                    new_position = maybe_box_pos + Position(1, 0)
                    movable_right = (not self._out_of_bounds(new_position)) and \
                        not (new_position in self.static_environment.environment and
                             not self.static_environment.environment[new_position].STEPPABLE)

                    new_position = maybe_box_pos + Position(-1, 0)
                    movable_left = (not self._out_of_bounds(new_position)) and \
                        not (new_position in self.static_environment.environment and
                             not self.static_environment.environment[new_position].STEPPABLE)

                    new_position = maybe_box_pos + Position(0, 1)
                    movable_down = (not self._out_of_bounds(new_position)) and \
                        not (new_position in self.static_environment.environment and
                             not self.static_environment.environment[new_position].STEPPABLE)

                    new_position = maybe_box_pos + Position(0, -1)
                    movable_up = (not self._out_of_bounds(new_position)) and \
                        not (new_position in self.static_environment.environment and
                             not self.static_environment.environment[new_position].STEPPABLE)

                    movable_both_x = movable_right and movable_left
                    movable_both_y = movable_up and movable_down

                    if movable_both_x or movable_both_y:
                        not_blocked_boxes += 1

        return not_blocked_boxes < goals

    # Returns if this level reached termination condition
    def is_cleared(self, curr_state):
        current_state = curr_state
        # For each goal position
        for pos in self.static_environment.goal_position:
            # If the position is not also the position of a box then we return false
            if pos not in current_state.environment or not isinstance(current_state.environment[pos], Box):
                return False
        # If all goals are filled with boxes then we reached termination condition
        return True

    # Returns if the provided move is valid
    def is_valid_move(self, curr_state: State, direction: Position):
        # We initialize variables to be used
        current_state = curr_state
        valid_move = True

        # We make sure the direction is supported
        if direction not in current_state.agent.ACCEPTED_ACTIONS:
            return False

        # We calculate the new position after executing the move
        new_position = current_state.agent.position + direction

        # case 0: new position is out of bounds
        if self._out_of_bounds(new_position):
            valid_move = False
        # case 1: there is a dynamic object (a box cuz new position is calculated from agent position)
        elif new_position in current_state.environment:
            # If box is not pushable
            if not self._is_pushable(current_state, pusher=current_state.agent,
                                     pushed=current_state.environment[new_position],
                                     pushed_position=new_position, direction=direction):
                # Then it's not a valid move
                valid_move = False
        # case 2: there is static object (Wall or goal), by definition, these aren't pushable
        elif new_position in self.static_environment.environment:
            obj = self.static_environment.environment[new_position]
            # If we cannot step on the object then its not a valid move
            if not obj.STEPPABLE:
                valid_move = False

        return valid_move

    # Returns a collection of valid moves from this state
    def get_valid_moves(self, curr_state: State):
        # Initializing variables to be used
        current_state = curr_state
        valid_moves = set()
        # We go through the accepted actions for an agent
        for direction in current_state.agent.ACCEPTED_ACTIONS:
            # If the action is valid then we add it to the collection
            if self.is_valid_move(curr_state, direction):
                valid_moves.add(direction)
        # Finally we return the collection
        return valid_moves

    # Auxiliary functions

    # Returns if the position is out of bounds
    def _out_of_bounds(self, new_pos):
        board_size = self.static_environment.board_size
        return new_pos.x < 0 or new_pos.y < 0 or new_pos.x >= board_size.x or new_pos.y >= board_size.y

    # Determines box object is pushable
    def _is_pushable(self, current_state, pusher, pushed, pushed_position, direction):

        # If object being pushed is steppable then it's pushable TODO never call this function with non box
        if pushed.STEPPABLE:
            return True

        # If object being pushed is not movable then false TODO never call this function with non box
        if not pushed.is_movable(pusher):
            return False

        # Position where the pushed object will end up in after being pushed
        new_position = pushed_position + direction

        # If the pushed object is out of bounds then object was not pushable
        if self._out_of_bounds(new_position):
            return False

        # If object being pushed would be pushed against another dynamic object (aka another box)
        if new_position in current_state.environment:
            # then it would not be pushable
            return False
        # If object being pushed would be pushed against a static object (aka wall or goal)
        elif new_position in self.static_environment.environment:
            # Then if its a wall its not pushable, if its a goal its pushable
            if isinstance(self.static_environment.environment[new_position], Goal):
                return True
            else:
                return False
        # If there is nothing, then its floor and its pushable
        return True

    # Changes state in parameter so the object in the pushed_position is moved on the asked direction
    def _push(self, curr_state, pushed_position, direction):
        # Initializing variables
        current_state = curr_state
        new_position = pushed_position + direction

        # If there is an object in the pushed position
        if pushed_position in current_state.environment:
            # and the object is steppable
            if current_state.environment[pushed_position].STEPPABLE:
                # then object could not be pushed and no changes made
                return current_state

            # A dynamic pushable object
            # current_state = self._push(current_state, new_position, direction) TODO this line makes no sense
            # Otherwise, we remove the object from the pushed position
            obj = current_state.environment.pop(pushed_position)
            # And add it again in the new position
            current_state.environment[new_position] = obj

        return current_state

    @classmethod
    def from_ascii(cls, ascii_map):
        # Ascii map should be valid, if something goes wrong we will throw an exception
        if not isinstance(ascii_map, str):
            raise ValueError('File did not contain a valid ASCII sokoban map. Input is not a string.')

        map_rows = 0
        map_columns = 0
        current_position = Position(0, 0)
        static_objects = dict()
        boxes = dict()
        agent_position = ''

        current_row_length = 0
        for symbol in ascii_map:
            # If it's a wall or a goal, we add it to the dict of static objects
            if symbol in Wall.ASCII_SYMBOL:
                static_objects[current_position] = Wall()
            elif symbol in Goal.ASCII_SYMBOL:
                static_objects[current_position] = Goal()
            # If it's a box, we add it to the dynamic dict
            elif symbol in Box.ASCII_SYMBOL:
                boxes[current_position] = Box()
            elif symbol in Box.ASCII_SYMBOL_GOAL:
                boxes[current_position] = Box()
                static_objects[current_position] = Goal()
            # If it's an agent, we save it in the agent variable
            elif symbol in Agent.ASCII_SYMBOL:
                agent_position = Agent(current_position)
            elif symbol in Agent.ASCII_SYMBOL_GOAL:
                agent_position = Agent(current_position)
                static_objects[current_position] = Goal()
            # If at this point it's not a blank space or a new line symbol, then something is wrong
            elif symbol not in ['\n', ' ']:
                raise ValueError('Detected a non valid ASCII character: ', symbol)

            # If \n then end of this row, we take this chance to keep track of map size
            if symbol == '\n':
                # If there were other symbols in the row, then we update position for next symbol
                if current_row_length != 0:
                    current_position = Position(0, current_position.y + 1)
                # If this row length is the largest so far, then we save it as map column size
                if current_row_length > map_columns:
                    map_columns = current_row_length
                # Since this is \n, we reset counter for next row
                current_row_length = 0
            else:
                # If current_row_length is 0, then we in a new map row
                if current_row_length == 0:
                    map_rows += 1
                # Since it's not \n, then we count this current symbol for the row length
                current_row_length += 1
                # And we update position of next symbol
                current_position = Position(current_position.x + 1, current_position.y)

        # We make sure we found an Agent
        if not isinstance(agent_position, Agent):
            raise ValueError('File did not contain a valid ASCII sokoban map. Could not find a player in the map.')

        # Now we create the GameBoard with the data we collected
        board_size = Position(map_columns, map_rows)
        static_environment = StaticEnvironment(board_size=board_size, environment=static_objects)
        current_state = State(agent=agent_position, environment=boxes)
        return cls(static_environment, current_state)
