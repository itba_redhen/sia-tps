# SIA TP 3 - Perceptrón Simple y Multicapa

## Dependencias Necesarias
* pip
* [pipenv](https://pipenv.pypa.io/en/latest/)

## Instalación y Uso
### Paso 0: Ir a la carpeta tp03
``
cd tp03
``

### Paso 1: Preparar el entorno
``
pipenv install
``

### Paso 2: Correr el programa
``
pipenv run python [ejercicio]
``

Donde [ejercicio] puede referirse a:
* ej1a.py
* ej1b.py
* ej2.py

Para los posibles parámetros de algunos ejercicios, consultar:

``
pipenv run python [ejercicio] -h
``