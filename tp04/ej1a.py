import getopt
import sys

from kohonen import Kohonen


def main(argv):
    # Parameters
    file_path = 'europe.csv'
    lr_zero: float = 0.8
    k: int = 3
    r_zero: float = 2
    i_factor: int = 500

    help_text = 'ej1b.py -f <file_path> -l <learning_rate> -k <dimension> -r <radius> -i <i_factor>'

    try:
        opts, args = getopt.getopt(argv, "hf:l:k:r:i:",
                                   [
                                       "file_path=",
                                       "learning_rate=",
                                       "dimension=",
                                       "radius=",
                                       "i_factor="
                                   ])
    except getopt.GetoptError:
        print(help_text)
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print(help_text)
            sys.exit()
        elif opt in ("-f", "--file_path"):
            file_path = arg
        elif opt in ("-l", "--learning_rate"):
            lr_zero = float(arg)
        elif opt in ("-k", "--dimension"):
            k = int(arg)
        elif opt in ("-r", "--radius"):
            r_zero = float(arg)
        elif opt in ("-i", "--i_factor"):
            i_factor = int(arg)

    # Start kohonen
    kohonen = Kohonen(
        lr_zero=lr_zero,
        k=k,
        r_zero=r_zero,
        i_factor=i_factor
    )

    # Train network
    kohonen.train(file_path=file_path)

    # Plot results & comparisons
    kohonen.plot_u_matrix()
    kohonen.plot_test(file_path=file_path)
    kohonen.plot_test_vs_pca(file_path=file_path)


if __name__ == "__main__":
    main(sys.argv[1:])
