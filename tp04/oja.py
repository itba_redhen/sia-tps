import time
import pandas as pd
import numpy as np
from numpy import random
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt


class Oja:

    # Init Oja
    # eta_zero: Starting learning Rate of the network, should be lower than 0.5
    # max_epochs: max amount of iterations of the entry data
    def __init__(self, eta_zero: float = 0.2, max_epochs: int = 2000):
        self.eta_zero = eta_zero
        self.max_epochs = max_epochs
        self.weight_history = None
        self.epochs = 0
        self.time = 0

    # Creates weights based on data on file
    def train(self, file_path: str):

        # Set up data
        col_header, country_names, data = self._get_normalized_data(file_path)
        countries_len = len(country_names)
        dims_len = len(col_header)
        max_epochs = self.max_epochs
        weight_history = []
        start_time = time.time()

        weights = random.uniform(-1, 1, dims_len)

        # Iterate epochs
        e = 0
        for e in range(1, max_epochs + 1):
            eta = self._get_eta(e)

            # Break if eta <= 0
            if eta <= 0:
                break

            # Iterate data
            for row in range(countries_len):
                current_row = data[row]
                # Get sum of the inner product of row & weights
                s = np.dot(current_row, weights)
                # Update weights
                weights += eta * s * (current_row - s * weights)
                # Normalize data every weight update
                weights = weights / np.linalg.norm(weights)

            weight_history.append(weights)

        # Saving results
        self.time = time.time() - start_time
        self.weight_history = np.array(weight_history)
        self.epochs = e
        return weights

    # Compares with real pca values
    def compare_with_pca(self, file_path: str):

        if self.weight_history is None:
            raise Exception("Training must be done beforehand")

        weight_history = self.weight_history

        # Set up data
        col_header, country_names, data = self._get_normalized_data(file_path)

        # Class to use to calculate primary components
        # get pc only
        pca = PCA(1)
        pca_start_time = time.time()
        pca.fit_transform(data)
        pca_time = time.time() - pca_start_time
        pc = pca.components_[0]

        # Flip pca if the values are the opposite to calculated value
        last_weight = weight_history[-1]
        if np.sum(np.abs(last_weight + pc)) < np.sum(np.abs(last_weight - pc)):
            pc *= -1

        # Calculate error for each epoch
        error = np.average(np.abs(weight_history - pc), axis=1)

        # Print comparisons
        print("Library Method first PC", pc, sep="\n")
        print("Oja Rule first PC", last_weight, sep="\n")
        print("Difference", pc - last_weight, sep="\n")

        # Print time comparison
        print("Library Method PCA time:\t{:.4f} seconds".format(pca_time))
        print("Oja Rule PC time:\t{:.4f} seconds".format(self.time))
        print("Oja Rule epochs:\t", self.epochs, sep="")
        print("Oja Rule PC error:\t", error[-1])

        # Init fig
        fig, ax = plt.subplots()

        # Plot titles
        ax.set_title('Difference between Oja PC & PCA function')
        plt.xlabel("Epoch")
        plt.ylabel("Average difference per dimension")

        # Plot
        ax.plot(error)

        # Logarithmic scale
        plt.yscale("log")

        # Show grids
        plt.grid()

        plt.show()
        return

    # Get lr based on epochs
    def _get_eta(self, e: int):
        return self.eta_zero / e

    # Normalized data
    @staticmethod
    def _get_normalized_data(file_path: str, delimiter=','):
        # Get data
        df = pd.read_csv(file_path, delimiter=delimiter)

        # Country names as array
        country_names = np.asarray(df['Country'])
        del df['Country']

        # Column data, after excluding Country
        col_header = list(df.columns.values)

        # Normalize data
        sc = StandardScaler()
        df_normalized = sc.fit_transform(df)

        return col_header, country_names, df_normalized
