import getopt
import sys

from oja import Oja


def main(argv):
    # Parameters
    file_path = 'europe.csv'
    eta_zero: float = 0.2
    max_epochs: int = 2000

    try:
        opts, args = getopt.getopt(argv, "hf:r:e:", ["file_path=", "learning_rate=", "epochs="])
    except getopt.GetoptError:
        print('ej1b.py -f <input_file> -r <learning_rate> -e <epochs>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('ej1b.py -f <input_file> -r <learning_rate> -e <epochs>')
            sys.exit()
        elif opt in ("-f", "--file_path"):
            file_path = arg
        elif opt in ("-r", "--learning_rate"):
            eta_zero = float(arg)
        elif opt in ("-e", "--epochs"):
            max_epochs = int(arg)

    # Start oja class
    oja = Oja(eta_zero=eta_zero, max_epochs=max_epochs)

    # Get the weight array
    oja.train(file_path)

    # Compare with library method
    oja.compare_with_pca(file_path)


if __name__ == "__main__":
    main(sys.argv[1:])
