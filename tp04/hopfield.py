import copy
import numpy as np


class Hopfield:
    # Initializes Hopfield network with a determined dimension
    def __init__(self, dimension: int, fd=None):
        self.dimension = dimension
        self.weights = np.zeros((dimension, dimension))
        self.fd = fd

    # Expects pattern of initialized dimension
    # Updates network weights to include this pattern
    def train(self, pattern: np.ndarray):
        # Holder for this training session
        weights_update = np.zeros((self.dimension, self.dimension))
        # Calculating the update
        for i in range(self.dimension):
            for j in range(self.dimension):
                if i != j:
                    weights_update[i][j] = (pattern[i] * pattern[j]) / float(self.dimension)

        # Applying update
        for i in range(self.dimension):
            for j in range(self.dimension):
                if i != j:
                    self.weights[i][j] += weights_update[i][j]

    # Expects query array of initialized dimension
    def run(self, query: np.ndarray):
        # State array S
        state = copy.deepcopy(query)
        # Iteration
        done = False
        while not done:
            # New state
            new = np.zeros(self.dimension)
            for i in range(self.dimension):
                aux = 0
                for j in range(self.dimension):
                    aux += self.weights[i][j] * state[j]
                new[i] = self.signum(aux)
            # Check if change
            if np.array_equal(state, new):
                done = True
            else:
                self.write(state)
                state = new
        # Once done
        self.write(state)
        return state

    @staticmethod
    def signum(number):
        if number < 0:
            return -1
        else:
            return 1

    # Writes a state to the fd
    def write(self, state: np.ndarray):
        if self.fd is not None:
            self.write_letter(state)
            self.fd.write('\n')

    def write_letter(self, letter: np.ndarray, dimension: int = 5):
        aux = ""
        for i in range(len(letter)):
            if i != 0 and i % dimension == 0:
                self.fd.write(aux)
                self.fd.write('\n')
                aux = ""
            if letter[i] == 1:
                aux += "*"
            else:
                aux += " "
        self.fd.write(aux)
        self.fd.write('\n')

    def print_weights(self):
        for row in self.weights:
            aux = ""
            for weight in row:
                if weight >= 0:
                    aux += '+'
                if weight == 0:
                    aux += "0.00" + '\t'
                else:
                    aux += str(round(weight, 2)) + '\t'
            print(aux)
