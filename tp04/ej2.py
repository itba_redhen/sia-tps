import copy
import random
import numpy as np
from hopfield import Hopfield
import matplotlib.pyplot as plt


def mutate(letter: np.ndarray, number_of_mutations: int):
    mutations_done = 0
    new_letter = copy.deepcopy(letter)
    while mutations_done < number_of_mutations:
        idx = random.randint(0, len(new_letter) - 1)
        if new_letter[idx] == -1:
            new_letter[idx] = 1
            mutations_done += 1
    return new_letter


if __name__ == '__main__':
    # We define the letter we are going to use
    j = np.array([1, 1, 1, 1, 1,
                  -1, -1, -1, 1, -1,
                  -1, -1, -1, 1, -1,
                  1, -1, -1, 1, -1,
                  1, 1, 1, -1, -1])
    k = np.array([1, -1, -1, 1, -1,
                  1, -1, 1, -1, -1,
                  1, 1, -1, -1, -1,
                  1, -1, 1, -1, -1,
                  1, -1, -1, 1, -1])
    c = np.array([1, 1, 1, 1, 1,
                  1, -1, -1, -1, -1,
                  1, -1, -1, -1, -1,
                  1, -1, -1, -1, -1,
                  1, 1, 1, 1, 1])
    y = np.array([1, -1, -1, -1, 1,
                  -1, 1, -1, 1, -1,
                  -1, -1, 1, -1, -1,
                  -1, -1, 1, -1, -1,
                  -1, -1, 1, -1, -1])
    # We open output file
    with open('ej2.out', 'w') as file:
        # We create network
        network = Hopfield(len(j), file)
        # We train it
        network.train(j)
        network.train(k)
        network.train(c)
        network.train(y)
        # We get weight's matrix and print it
        plt.matshow(network.weights, cmap=plt.cm.get_cmap("BrBG"))
        plt.show()
        # We mutate the letters a bit and run the network
        network.run(mutate(j, 3))
        network.run(mutate(k, 3))
        network.run(mutate(c, 3))
        network.run(mutate(y, 3))
        # We mutate a letter a lot to add noise and run it again
        network.run(mutate(j, 10))
