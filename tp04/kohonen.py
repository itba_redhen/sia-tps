import pandas as pd
import math
import numpy as np
from numpy import random
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt


class Kohonen:

    # Init network
    # lr_zero: Starting learning Rate of the network
    # k: Dimension of the 2d grid
    # r_zero: Initial Radius
    # i_factor: Iteration Factor, the number of training consists of i_factor * len(training_set)
    def __init__(self,
                 lr_zero: float = 0.8,
                 k: int = 3,
                 r_zero: float = 2,
                 i_factor: int = 500
                 ):
        self.lr_zero = lr_zero
        self.k = k
        self.r_zero = r_zero
        self.i_factor = i_factor
        self.weights = None
        self.iterations = 0
        self.u_matrix = None

    # Creates weight & u matrix based on data on file
    def train(self, file_path: str):

        # Set up data
        col_header, country_names, data = self._get_normalized_data(file_path)
        countries_len = len(country_names)
        dims_len = len(col_header)

        # Start neurons
        k = self.k
        weights = np.zeros((k, k, dims_len))
        # fill with random inputs from data
        for i in range(k):
            for j in range(k):
                random_x_i = random.randint(countries_len)
                weights[i][j] = data[random_x_i]

        # Iterative step
        epochs = self.i_factor * countries_len
        epoch_iterator = list(range(countries_len))
        t = 0
        for t in range(1, epochs + 1):

            # set values
            lr = self._get_lr(t)
            radius = self._get_radius(t)

            # break in case of 0 learning rate
            if lr <= 0:
                break

            # Shuffling the order of the countries
            random.shuffle(epoch_iterator)
            for random_x_i in epoch_iterator:
                # select a value from data
                random_x = data[random_x_i]

                # get the index weight with lowest value
                min_distance_index_i, min_distance_index_j = self._get_closest_neuron(weights, random_x)

                # update weights
                for i in range(k):
                    for j in range(k):
                        # Check if within radius
                        if self._get_neuron_distance(min_distance_index_i, min_distance_index_j, i, j) <= radius:
                            # Update weight
                            weights[i][j] += lr * (random_x - weights[i][j])

        # saving training data
        self.weights = weights
        self.iterations = t

        # creating u_matrix
        self.u_matrix = self._create_u_matrix(weights)

        return

    # Get iteration's learning rate
    def _get_lr(self, t: int):
        return self.lr_zero / t

    # Get iteration's radius
    def _get_radius(self, t: int):
        return max(self.r_zero / t, 1)

    # Gets how close it is to the real value
    # Using euclidean distance
    @staticmethod
    def _get_similitude(v_1, v_2):
        return np.sqrt(np.sum(np.square(v_1 - v_2)))

    # Gets the closest neuron
    def _get_closest_neuron(self, weights, x):
        # get the index weight with lowest value
        k = self.k
        min_distance = math.inf
        min_distance_index_i = -1
        min_distance_index_j = -1
        for i in range(k):
            for j in range(k):
                current_distance = self._get_similitude(weights[i][j], x)
                # save the current value if it is the lowest
                if current_distance < min_distance:
                    min_distance = current_distance
                    min_distance_index_i = i
                    min_distance_index_j = j

        return min_distance_index_i, min_distance_index_j

    # Gets the distance between 2 neurons
    # Using euclidean distance
    @staticmethod
    def _get_neuron_distance(x_1, y_1, x_2, y_2):
        return np.sqrt(np.sum(np.square([x_1 - x_2, y_1 - y_2])))

    # Creates u_matrix from weights matrix
    def _create_u_matrix(self, weights):
        # creating base matrix
        k = self.k
        u_matrix = np.zeros((k, k), np.float64)

        # calculating values
        for i in range(k):
            for j in range(k):
                u_matrix[i][j] = self._get_distance_sum(weights, i, j)

        return u_matrix

    # Gets the averaged sum of the distance to neighbours
    def _get_distance_sum(self, weights, i, j):

        # Setting max & min
        min_x = min_y = 0
        max_x = max_y = self.k

        # Get the distance to neighbours
        dist = []
        if i - 1 >= min_x:
            dist.append(self._get_similitude(weights[i][j], weights[i - 1][j]))
        if i + 1 < max_x:
            dist.append(self._get_similitude(weights[i][j], weights[i + 1][j]))
        if j - 1 >= min_y:
            dist.append(self._get_similitude(weights[i][j], weights[i][j - 1]))
        if j + 1 < max_y:
            dist.append(self._get_similitude(weights[i][j], weights[i][j + 1]))

        # Return average
        return np.average(dist)

    # Normalized data
    @staticmethod
    def _get_normalized_data(file_path: str, delimiter=','):
        # Get data
        df = pd.read_csv(file_path, delimiter=delimiter)

        # Country names as array
        country_names = np.asarray(df['Country'])
        del df['Country']

        # Column data, after excluding Country
        col_header = list(df.columns.values)

        # Normalize data
        sc = StandardScaler()
        df_normalized = sc.fit_transform(df)

        return col_header, country_names, df_normalized

    # Test
    def test(self, file_path: str):

        # must be called after training
        if self.weights is None:
            raise Exception("Test must be called after training")

        # set up data
        col_header, country_names, data = self._get_normalized_data(file_path)
        countries_len = len(country_names)
        weights = self.weights

        # create list
        countries_data = []
        for c in range(countries_len):
            closest_neuron_i, closest_neuron_j = self._get_closest_neuron(weights, data[c])
            countries_data.append([closest_neuron_i, closest_neuron_j, country_names[c]])

        return np.array(countries_data)

    # Plot u_matrix
    def plot_u_matrix(self):
        plt.matshow(self.u_matrix, cmap='Blues_r')
        plt.colorbar()
        plt.title("U Matrix")
        plt.show()
        pass

    # Plot the frequency of the test
    def plot_test(self, file_path: str):

        # must be called after training
        if self.weights is None:
            raise Exception("Test must be called after training")

        # set up data
        col_header, country_names, data = self._get_normalized_data(file_path)
        countries_len = len(country_names)
        weights = self.weights
        k = self.k

        # create list with frequencies
        countries_freq = np.zeros((k, k), dtype=int)
        countries = np.empty((k, k), dtype=object)
        for c in range(countries_len):
            closest_neuron_i, closest_neuron_j = self._get_closest_neuron(weights, data[c])
            countries_freq[closest_neuron_i][closest_neuron_j] += 1
            if countries[closest_neuron_i][closest_neuron_j] is None:
                countries[closest_neuron_i][closest_neuron_j] = []
            countries[closest_neuron_i][closest_neuron_j].append(country_names[c])

        # print groups
        group_id = 1
        for i in range(k):
            for j in range(k):
                if countries[i][j] is not None:
                    print("Group {:d} [{:d},{:d}]".format(group_id, i, j))
                    for c in countries[i][j]:
                        print(c)
                    print("")
                    group_id += 1

        fig, ax = plt.subplots()
        im = ax.imshow(countries_freq, cmap='Greens')
        ax.set_title("Frequency Heatmap")

        # Loop over data dimensions and create text annotations.
        for i in range(k):
            for j in range(k):
                if countries_freq[i, j] > 0:
                    ax.text(j, i, countries_freq[i, j], ha="center", va="center", color="w")

        # Create colorbar
        color_bar = ax.figure.colorbar(im, ax=ax)
        color_bar.ax.set_ylabel("Frequency", rotation=-90, va="bottom")

        plt.show()
        pass

    # PCA plot coloured by kohonen test results
    def plot_test_vs_pca(self, file_path: str):

        # must be called after training
        if self.weights is None:
            raise Exception("Test must be called after training")

        # set up data
        col_header, country_names, data = self._get_normalized_data(file_path)
        countries_len = len(country_names)
        weights = self.weights
        k = self.k

        # color array
        color_array = []
        blue_value = 200
        for c in range(countries_len):
            closest_neuron_i, closest_neuron_j = self._get_closest_neuron(weights, data[c])
            color_array.append((closest_neuron_i / k, closest_neuron_j / k, blue_value / 255))
        color_array = np.array(color_array)

        # Class to use to calculate primary components
        # 2 components for biplot
        pca = PCA(2)

        # Values obtained through pca
        values = pca.fit_transform(data)

        # PCA results to variables
        components = pca.components_
        variance_proportion = pca.explained_variance_ratio_

        # Init fig
        fig, ax = plt.subplots()

        # Plot title
        ax.set_title('Biplot of PC 1 & PC 2 | Europe')

        # Labels
        ax.set_xlabel("PC 1 ({:.2f}%)".format(variance_proportion[0] * 100))
        ax.set_ylabel("PC 2 ({:.2f}%)".format(variance_proportion[1] * 100))

        # Scatter plot with labels
        scatter_x = values[:, 0]
        scatter_y = values[:, 1]
        scatter_labels = country_names
        x_delta = y_delta = 0.05
        for i in range(len(scatter_labels)):
            ax.scatter(x=scatter_x[i], y=scatter_y[i], color=color_array[i])
            ax.text(scatter_x[i] + x_delta, scatter_y[i] + y_delta, scatter_labels[i])

        # Arrow plot
        arrow_x = components[0]
        arrow_y = components[1]
        arrow_labels = col_header
        for i in range(len(arrow_labels)):
            ax.arrow(0, 0, arrow_x[i], arrow_y[i], color="blue")

        # Adding grids
        ax.yaxis.grid(True)
        ax.xaxis.grid(True)

        # Display Biplot
        plt.show()

        pass
