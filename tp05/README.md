# SIA TP 5 - Deep Learning

## Dependencias Necesarias
* pip
* [pipenv](https://pipenv.pypa.io/en/latest/)

## Instalación y Uso
### Paso 0: Ir a la carpeta tp05
``
cd tp05
``

### Paso 1: Preparar el entorno
``
pipenv install
``

### Paso 2: Modificar archivo de configuraciones
Disponible desde:
``
app.conf
``

### Paso 3: Correr el programa principal
``
pipenv run python app.py
``