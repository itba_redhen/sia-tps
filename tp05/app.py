import sys
from config import Config
from autoencoder import *
from fonts import Fonts


def main(argv):
    config = Config.from_file()

    # ae = Autoencoder(config.lr, config.encoder_structure, config.decoder_structure, config.epochs, config.p_key)
    ae = DenoisingAutoencoder(config.lr, config.encoder_structure, config.decoder_structure, config.epochs,
                              config.p_key, noise=config.noise)

    ae.train(config.file_path)

    # For generation
    # letters, input_data = Fonts.get_data(config.file_path)
    # ae.generate_random(input_data, letters, 20)

    ae.test(config.file_path)


if __name__ == '__main__':
    main(sys.argv[1:])
