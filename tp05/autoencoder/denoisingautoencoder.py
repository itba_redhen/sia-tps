from .autoencoder import Autoencoder
import matplotlib.pyplot as plt

from perceptron import *
from fonts import Fonts
import numpy as np
from numpy import random


# Basic autoencoder with 1 hidden layer
class DenoisingAutoencoder(Autoencoder):

    def __init__(self, lr: float = 1, encoder_structure=[2, 25, 20], decoder_structure=[2, 25, 35], epochs: int = 5000,
                 p_key='non-linear-with-one-entry-delta-w', beta: float = 0.5, noise: int = 10):
        super().__init__(lr, encoder_structure, decoder_structure, epochs, p_key, beta)

        self.noise = noise

    def test(self, file_path: str):
        # Set up data
        letters, data = Fonts.get_data(file_path)
        data_rows, data_cols = data.shape

        output_data = self.encoder.test(data)
        x_dash = self.decoder.test(output_data)

        difference = list()
        for i in range(data_rows):
            current_difference = np.sum(np.abs(np.round(x_dash[i]) - data[i]))
            difference.append(current_difference)
            print("\nLetter", letters[i], sep='\t')
            print("Absolute Difference:", current_difference, sep='\t')
            Fonts.display_letter(Fonts.num_array_to_letter(x_dash[i]))
        max_diff = np.max(difference)

        noised_input = self._preprocess(data)
        noised_output = self.encoder.test(noised_input)
        noised_scatter_x = noised_output[:, 0]
        noised_scatter_y = noised_output[:, 1]

        # Init fig
        fig, ax = plt.subplots()

        # Plot title
        ax.set_title('Biplot of Latent Space')

        # Labels
        ax.set_xlabel("Perceptron 1")
        ax.set_ylabel("Perceptron 2")

        # Scatter plot with labels
        scatter_x = output_data[:, 0]
        scatter_y = output_data[:, 1]
        scatter_labels = letters
        x_delta = y_delta = 0.005
        for i in range(len(scatter_labels)):
            ax.scatter(x=noised_scatter_x[i], y=noised_scatter_y[i], color='red', s=9)
            ax.scatter(x=scatter_x[i], y=scatter_y[i], color=str(difference[i] / max_diff), s=5)
            ax.text(noised_scatter_x[i] + x_delta, noised_scatter_y[i] + y_delta, scatter_labels[i], color='red')
            ax.text(scatter_x[i] + x_delta, scatter_y[i] + y_delta, scatter_labels[i])

        # Adding grids
        ax.yaxis.grid(True)
        ax.xaxis.grid(True)

        # Display Biplot
        plt.show()

        return output_data

    # Changes to make before training the input
    def _preprocess(self, x: np.ndarray):
        # Add noise to input
        x_noise = x
        for _ in range(self.noise):
            sign = random.randint(0, 2)
            modify_index = random.randint(0, len(x))
            x_noise[modify_index] = sign

        return x
