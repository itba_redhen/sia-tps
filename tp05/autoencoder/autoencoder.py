import matplotlib.pyplot as plt

from perceptron import *
import numpy as np
from numpy import random
from tp05.fonts import Fonts


# Basic autoencoder with 1 hidden layer
class Autoencoder:

    def __init__(self, lr: float = 0.3, encoder_structure=[2, 14, 2], decoder_structure=[2, 14, 35], epochs: int = 5000,
                 p_key='non-linear-with-one-entry-delta-w', beta: float = 0.5):
        self.lr = lr

        self.input_p = decoder_structure[-1]
        self.hidden_p = encoder_structure[-1]
        self.epochs = epochs

        # Encoder & Decoder
        encoder_structure = np.array(encoder_structure)
        encoder = MultiLayerPerceptron(encoder_structure, lr, epochs, p_key=p_key, beta=beta)
        encoder.initialize_weights(self.input_p + 1)
        decoder_structure = np.array(decoder_structure)
        decoder = MultiLayerPerceptron(decoder_structure, lr, epochs, p_key=p_key, beta=beta)
        decoder.initialize_weights(self.hidden_p + 1)
        self.encoder = encoder
        self.decoder = decoder
        pass

    def train(self, file_path: str):
        # Set up data
        letters, data = Fonts.get_data(file_path)
        data_rows, data_cols = data.shape

        if data_cols != self.input_p:
            raise Exception("data cols doesn't match input cols")

        epochs = self.epochs
        epoch_iterator = list(range(data_rows))

        # Assigning encoder & decoder to local variables
        encoder = self.encoder
        decoder = self.decoder

        for t in range(1, epochs + 1):
            # Shuffling the order of the elements
            random.shuffle(epoch_iterator)
            epoch_error = 0
            for random_x_i in epoch_iterator:
                # select a value from data
                random_x = self._preprocess(data[random_x_i])

                # Forward propagate
                z = encoder.train_single(random_x)
                x_dash = decoder.train_single(z)

                # Compare x_dash with x
                epoch_error += np.sqrt(np.sum(np.square(random_x - x_dash)))

                # Back propagate
                expected_z = decoder.update_weights(random_x)
                encoder.update_weights(expected_z)
            print("epoch", t, "\terror:", epoch_error)

        pass

    def encode(self, x: np.ndarray):
        return self.encoder.test(x)[0]

    def test(self, file_path: str):
        # Set up data
        letters, data = Fonts.get_data(file_path)
        data_rows, data_cols = data.shape

        output_data = self.encoder.test(data)
        x_dash = self.decoder.test(output_data)

        difference = list()
        for i in range(data_rows):
            current_difference = np.sum(np.abs(np.round(x_dash[i]) - data[i]))
            difference.append(current_difference)
            print("\nLetter", letters[i], sep='\t')
            print("Absolute Difference:", current_difference, sep='\t')
            Fonts.display_letter(Fonts.num_array_to_letter(x_dash[i]))
        max_diff = np.max(difference)

        # Init fig
        fig, ax = plt.subplots()

        # Plot title
        ax.set_title('Biplot of Latent Space')

        # Labels
        ax.set_xlabel("Perceptron 1")
        ax.set_ylabel("Perceptron 2")

        # Scatter plot with labels
        scatter_x = output_data[:, 0]
        scatter_y = output_data[:, 1]
        scatter_labels = letters
        x_delta = y_delta = 0.01
        for i in range(len(scatter_labels)):
            ax.scatter(x=scatter_x[i], y=scatter_y[i], color=str(difference[i] / max_diff))
            ax.text(scatter_x[i] + x_delta, scatter_y[i] + y_delta, scatter_labels[i])

        # Adding grids
        ax.yaxis.grid(True)
        ax.xaxis.grid(True)

        # Display Biplot
        plt.show()

        return output_data

    def generate_random(self, x: np.ndarray, letter_array: np.ndarray, count: int = 1):

        for i in range(count):
            print("\nLetter number", i)
            x_len = len(letter_array)
            letters = random.randint(0, x_len, 2)
            l1 = letters[0]
            l2 = letters[1]
            print("l1:", letter_array[l1], sep='\t')
            print("l2:", letter_array[l2], sep='\t')
            self.generate(x[l1], x[l2])

    # Generates a new letter from the trained set
    def generate(self, l1, l2, distance: float = 0.5):

        # Encode both
        z1 = self.encode(l1)
        z2 = self.encode(l2)

        # Value at distance from z1
        zi = z1 + distance * (z2 - z1)

        decoded = self.decoder.test(zi)[0]

        print("Generated letter:")
        decoded_letter = Fonts.display_letter(Fonts.num_array_to_letter(decoded))

        return decoded_letter

    # Changes to make before training the input
    def _preprocess(self, x: np.ndarray):
        return x
