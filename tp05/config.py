from configparser import ConfigParser


class Config:
    config_path = 'app.conf'
    config_section = 'app-config'
    valid_pkeys = ['non-linear-with-one-entry-delta-w']

    def __init__(self, lr: float, encoder_structure: list, decoder_structure: list, epochs: int, noise: int, p_key: str,
                 file_path: str):
        self.lr = lr
        self.encoder_structure = encoder_structure
        self.decoder_structure = decoder_structure
        self.epochs = epochs
        self.noise = noise
        self.p_key = p_key
        self.file_path = file_path

    @classmethod
    def from_file(cls):
        # Open file for config parser
        f = open(cls.config_path, 'r')
        config = ConfigParser()
        config.read_file(f)
        # Close file
        f.close()
        # We go parameter by parameter initializing variables
        lr = float(config[cls.config_section]['lr'])
        if not lr > 0:
            raise ValueError('Learning rate must be a positive float.')

        encoder_structure = cls.__get_structure(config, 'encoder_structure')

        decoder_structure = cls.__get_structure(config, 'decoder_structure')

        noise = int(config[cls.config_section]['noise'])
        if not noise >= 0:
            raise ValueError("Noise must be a positive integer")

        epochs = int(config[cls.config_section]['epochs'])
        if not epochs > 0:
            raise ValueError("Epochs must be a positive integer")

        p_key = config[cls.config_section]['p_key']
        print(p_key)
        if p_key not in cls.valid_pkeys:
            raise ValueError("p_key is not valid")

        file_path = config[cls.config_section]['file_path']
        if len(file_path) == 0:
            raise ValueError("file_path must not be empty")
        return cls(lr, encoder_structure, decoder_structure, epochs, noise, p_key, file_path)

    @classmethod
    def __get_structure(cls, config, structure_name):
        structure = config[cls.config_section][structure_name]
        structure = structure.split(' ')
        number_of_layers = int(structure[0])
        structure_array = list()
        for number in structure:
            if int(number) <= 0:
                raise ValueError('Cant have negative or zero number of nodes in a layer')
            structure_array.append(int(number))
        if len(structure_array) - 1 != number_of_layers:
            raise ValueError('Expected ' + str(number_of_layers) + ' An in structure')
        return structure_array
