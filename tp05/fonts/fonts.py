import csv

import numpy as np


class Fonts:

    # Normalized data
    @staticmethod
    def get_data(file_path: str, delimiter='\t'):
        input_data = list()
        letter = list()
        # Get data
        with open(file_path, 'r', newline='') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=delimiter, quotechar=None)
            for row in csv_reader:
                letter.append(row[0])
                input_row = list()
                for v in row[1:]:
                    val = str(bin(int(str(v).strip(), 16))).replace('0b', '').zfill(5)
                    for c in val:
                        input_row.append(int(c))
                input_data.append(input_row)
        return letter, np.array(input_data)

    # Display letter, letter is a ndarray with hex strings
    @staticmethod
    def display_letter(letter: np.ndarray):
        values = list(map(lambda x: bin(int(x, 16)).replace('0b', '').zfill(5), letter))
        letter_array = list()
        for row in values:
            letter_row = list()
            for j in row:
                letter_row.append(j)
            letter_array.append(letter_row)

        for i in letter_array:
            for j in i:
                if j == '1':
                    val = "*"
                else:
                    val = " "
                print(val, end="")
            print("\n", end="")

        return np.array(letter_array)

    @staticmethod
    def num_array_to_letter(arr: np.ndarray):
        letters = arr.reshape((7, 5))
        letter_array = list()
        letter_len = 5
        for letter in letters:
            n = 0
            for i in range(letter_len):
                n += round(letter[i]) * pow(2, i)
            letter_array.append(hex(int(n)))
        return np.array(letter_array)
