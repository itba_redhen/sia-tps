import numpy as np
import math
from .linearperceptron import LinearPerceptron


class NonLinearPerceptron(LinearPerceptron):
    key = "non-linear-with-one-entry-delta-w"

    def __init__(self, lr: float = 0.1, max_iterations: int = 1000, weight_reset: int = None, fd=None, beta: float = 1):
        # Initializing parent class
        super().__init__(lr, max_iterations, weight_reset, fd)
        # Adding beta for sigmoid function
        self.beta = beta

    # Activation method, which depends on perceptron type
    def _activation(self, h):
        # Uses sigmoid for activation
        return math.tanh(self.beta * h)

    # Derivative of the activation function
    def _activation_derivative(self, h):
        # Derivative of the g(x) = tanh(B * x) function
        return self.beta * (1 - math.pow(math.tanh(h), 2))
