from perceptron import *
import math
import numpy as np
from numpy import random


class MultiLayerPerceptron:
    key = "multi-layer-perceptron"

    # structure is an array with the structure of the layers [N, a1, a2, ..., an]
    def __init__(self, structure, lr: float = 0.01, max_iterations: int = 1000, weight_reset: int = None, fd=None,
                 p_key: str = '', beta: float = 0.5):
        # Common attributes
        self.lr = lr
        self.max_iterations = max_iterations
        self.weight_reset = weight_reset
        self.fd = fd
        self.weights_initialized = False
        self.min_error = None
        # Size of network is the number of hidden layers + 1 for the output
        m = structure[0]
        # Holder for neuron matrix
        neurons = list()
        # We go through the layers
        for i in range(m):
            # Size of this layer
            size = structure[i + 1]
            # We create the array of nodes in this layer
            nodes = list()
            for j in range(size):
                nodes.append(self._get_perceptron_from_key(p_key, lr, weight_reset, fd))
            # We add the array to our neuron matrix
            neurons.append(nodes)
        # Finally we save neurons to instance
        self.neurons = neurons

        # Values for training and updating errors
        self.node_h_array = None
        self.node_output_array = None
        self.node_input_array = None
        pass

    # param x, y as input & output
    def train(self, x: np.ndarray, y: np.ndarray, epoch: int):
        # Adding bias column
        x_wb = np.insert(x, 0, values=1, axis=1)

        # Getting shape data
        x_rows, x_cols = x_wb.shape

        # File as local variable
        fd = self.fd

        # Max iterations & weight_reset to local variables
        max_iterations = self.max_iterations
        neurons = self.neurons

        # x & y must have same length
        if x_rows != len(y):
            raise ValueError("X and Y have different lengths")

        # Initialization of weights
        self.initialize_weights(x_cols)

        # Holder for number of iterations
        loop_i = 0
        # Highest value for error
        min_error = self.min_error
        if min_error is None:
            min_error = math.inf
        # Now we go through the training loop
        while loop_i < max_iterations and min_error > 0:
            # We get a random member of the training set
            index = random.randint(x_rows)

            # Now we propagate input through neuron network
            self.train_single(x_wb[index])

            # Update weights
            self.update_weights(y[index])

            # We calculate new error
            error = self._get_error(x, y)
            if error < min_error:
                min_error = error
            # Print to file
            if fd is not None:
                self.__print_error_to_file(fd, loop_i, error, min_error, epoch)
            # We update loop index (iteration counter)
            loop_i += 1

        # Update min error
        self.min_error = min_error
        # Loop info
        print("Epoch", epoch, ": Iterated", loop_i, "times")
        print("Stopped because:", end="\t")
        if min_error == 0:
            print("Error is 0")
        else:
            print("Reached max iterations")

    def train_single(self, x: np.ndarray):
        # Holders for the excitation and activation values of each node
        node_h_array = list()
        node_output_array = list()
        node_input_array = list()

        x_wb = np.insert(x, 0, values=1)

        # Now we propagate input through neuron network
        input_for_layer = x_wb
        neurons = self.neurons
        node_input_array.append(input_for_layer)
        for layer in neurons:
            # Rows of the node_h_array and node_output_array
            node_h_row = list()
            node_output_row = list()
            # Holder for the input for the next layer
            input_for_next_layer = list()
            # Add bias to the holder
            input_for_next_layer.append(1)
            for node in layer:
                # We calculate the excitation (h) value based on input for this layer
                node_h = node.excitation(input_for_layer)
                node_h_row.append(node_h)
                # We calculate the activation for this node
                node_output = node.activation(node_h)
                node_output_row.append(node_output)
                # We add this value for the input of the next layer
                input_for_next_layer.append(node_output)
            # Before going to next layer we update input converting to numpy array
            input_for_layer = np.array(input_for_next_layer)
            node_input_array.append(input_for_layer)
            # We also have to append the row to the node results arrays
            node_h_array.append(node_h_row)
            node_output_array.append(node_output_row)

        # Update node arrays
        self.node_h_array = node_h_array
        self.node_output_array = node_output_array
        self.node_input_array = node_input_array

        # Value returned by training
        return self.node_output_array[-1]

    def update_weights(self, real_y: np.ndarray):

        node_h_array = self.node_h_array
        node_output_array = self.node_output_array
        node_input_array = self.node_input_array

        neurons = self.neurons

        # Helper variables for weight adjustment loop
        done_outer_layer = False
        parents = None
        i = len(neurons)
        # Now we go backward in the neuron structure and update the weights
        for layer in neurons[::-1]:
            # Since we are going backwards, we should decrease i on each layer iteration
            i -= 1
            # Input and parents is shared among nodes in same layer
            node_input = np.array(node_input_array[i])
            new_parents = list()
            # We dont need to reverse the nodes in each layer, just the layers
            j = 0
            for node in layer:
                # We get excitation value and activation value from our saved ones
                node_h = node_h_array[i][j]
                node_output = node_output_array[i][j]
                # Since outer layer has a different delta calculation we check for it
                if not done_outer_layer:
                    # Outer layer delta
                    node_delta = node.activation_derivative(node_h) * (real_y[j] - node_output)
                    pass
                # For nodes in hidden layer the logic is different
                else:
                    # We calculate sum(weightpj*node_deltap) where p is parents index and j is node index
                    parents_sum = 0
                    for parent in parents:
                        # We weight that connects to this node TODO see if it's j or j+1 cuz bias
                        weight = parent[0].weights[j + 1]
                        # We get the node_delta of the parent
                        delta = parent[1]
                        # We add to the sum
                        parents_sum += weight * delta
                    # Hidden layers deltas
                    node_delta = node.activation_derivative(node_h) * parents_sum
                    pass
                # We calculate delta w for this node
                delta_w = self.lr * node_delta * node_input
                # We update the weights
                node.weights += delta_w
                # We save this node and its node_delta as a parent for next layer
                new_parents.append([node, node_delta])
                # We update j counter to get excitation values and activation values of next node in layer
                j += 1
            # We make sure to mark outer layers a complete before going to next layer
            if not done_outer_layer:
                done_outer_layer = True
            # We update parents for next layer
            parents = new_parents

        return np.fromiter(map(lambda p: p[1], parents), dtype=np.float64)

    # Testing implementation
    def test(self, x: np.ndarray) -> np.ndarray:
        # Sanity check
        if not self.weights_initialized:
            raise Exception('Call to test function with weights not initialized.')
        # Adding bias column
        x_wb = np.insert(x, 0, values=1, axis=len(x.shape) - 1)
        if len(x.shape) == 1:
            x_wb = np.array([x_wb])
        # Neurons as local variable
        neurons = self.neurons
        # Put each entry through the neuron network using saved weights
        calculated_values = list()
        for example in x_wb:
            # Now we propagate input through neuron network
            input_for_layer = example
            node_output = None
            for layer in neurons:
                # Holder for the input for the next layer
                input_for_next_layer = list()
                # Add bias to the holder
                input_for_next_layer.append(1)
                for node in layer:
                    # We calculate the excitation (h) value based on input for this layer
                    node_h = node.excitation(input_for_layer)
                    # We calculate the activation for this node
                    node_output = node.activation(node_h)
                    # We add this value for the input of the next layer
                    input_for_next_layer.append(node_output)
                # Before going to next layer we update input converting to numpy array
                input_for_layer = np.array(input_for_next_layer)
            # Now we save this result
            if node_output is None:
                raise Exception('Did not calculate node_output')
            calculated_value = input_for_layer[1:]
            calculated_values.append(calculated_value)
        # Now we return the calculated_values
        return np.array(calculated_values)

    # Calculates error based on distance to real value, y is expected value
    # TODO see if this should be the error function or the 0.5 * etc etc one
    def _get_error(self, x, y):
        # Calculate the value
        predicted_value = self.test(x)
        # Compare calculated value with testing one, abs to accumulate error values
        return np.sum(np.abs(np.subtract(y, predicted_value)))

    # Public get error
    def get_error(self, x, y):
        # Weights
        w = self.weights_initialized
        if not w:
            raise Exception("Call to perceptron without weights.")
        return self._get_error(x, y)

    # Calculate accuracy
    def accuracy(self, x: np.ndarray, y: np.ndarray, min_error: float = 0.001):
        # Calculate the values
        predicted_values = self.test(x)
        # We initialize variables
        t = f = 0
        # We calculate the variables
        if len(predicted_values) != len(y):
            raise Exception("Something went wrong")
        for i in range(len(predicted_values)):
            # If they match then it's a true something
            if abs(predicted_values[i] - y[i]) < min_error:
                t += 1
            else:
                f += 1
        # We make sure data makes sense
        if t + f != len(y):
            raise Exception("Trues + False was not same size as dataset")
        # We return accuracy
        return float(t) / (f + t)

    # Resets weights
    def reset_weights(self):
        # We gotta reset weights for each neuron
        neurons = self.neurons
        for layer in neurons:
            for node in layer:
                node.reset_weights()
        # And change initialized flag
        self.weights_initialized = False
        # Also reset error since weights were reset
        self.min_error = None

    # Initialize weights
    def initialize_weights(self, x_cols: int):
        # Initialization of weights
        if not self.weights_initialized:
            self.weights_initialized = True
            size_of_input = x_cols
            neurons = self.neurons
            for layer in neurons:
                for node in layer:
                    # We create the weights based on the size of the input with bias of the previous layer
                    node.weights = random.uniform(-0.5, 0.5, size_of_input)
                # Next layer will have this layer as input, so size of weights should be updated accordingly
                size_of_input = len(layer) + 1

    # Output the error value to a fd
    @staticmethod
    def __print_error_to_file(fd, iteration, current_error, min_error, epoch):
        fd.write(
            str(epoch) + "\t" +
            str(iteration) + "\t" +
            str(current_error) + "\t" +
            str(min_error) + "\n"
        )

    # get a perceptron based on the key
    @staticmethod
    def _get_perceptron_from_key(key: str, lr: float, weight_reset: int, fd, beta: float = 0.5):
        if key == 'linear-with-one-entry-delta-w':
            p = LinearPerceptron(lr=lr, weight_reset=weight_reset, fd=fd)
        elif key == 'non-linear-with-one-entry-delta-w':
            p = NonLinearPerceptron(lr=lr, weight_reset=weight_reset, fd=fd, beta=beta)
        else:
            p = Perceptron(lr=lr, weight_reset=weight_reset, fd=fd)
        return p
