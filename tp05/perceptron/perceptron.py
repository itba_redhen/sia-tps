import math
import numpy as np
from numpy import random


class Perceptron:
    key = "simple-with-step-activation"

    # param lr as learning rate
    # param max_iterations as max number of iterations
    # param weight_reset indicates the frequency relative to the training set size in which it resets the w array
    def __init__(self, lr: float = 0.1, max_iterations: int = 1000, weight_reset: int = None, fd=None):
        self.weights = None
        self.error = None
        self.lr = lr
        self.max_iterations = max_iterations
        self.weight_reset = weight_reset
        self.fd = fd

    # param x, y as input (2d array) & output (1d array) and epoch is the current epoch
    def train(self, x: np.ndarray, y: np.ndarray, epoch: int):

        # Adding bias column
        x_wb = np.insert(x, 0, values=1, axis=1)

        # Highest value for error
        min_error = math.inf if self.error is None else self.error
        # Weight array for lowest error
        min_weights = self.weights

        # Getting shape data
        x_rows, x_cols = x_wb.shape

        # File as local variable
        fd = self.fd

        # Max iterations & weight_reset to local variables
        max_iterations = self.max_iterations
        weight_reset = self.weight_reset

        # x & y must have same length
        if x_rows != len(y):
            raise Exception("X and Y have different lengths")

        # initiate weights, adding 1 col for bias
        weights = self.weights
        if weights is None:
            weights = random.uniform(-1, 1, x_cols)

        # i as current number of iterations
        i = 0
        while i < max_iterations and min_error > 0:

            # "Reset" weights every weight_reset*x_rows iterations
            if weight_reset is not None and (i % (weight_reset * x_rows)) == 0:
                weights = random.uniform(-1, 1, x_cols)

            # Getting the index to work with
            index = random.randint(x_rows)

            # Update weights
            delta_w = self._get_delta_w(x_wb, y, weights, index)
            weights = weights + delta_w

            # getting current error
            # get error also adds the bias so we send x without bias
            error = self._get_error(x, y, weights)

            # Updating min weights if lower value
            if error < min_error:
                min_error = error
                min_weights = weights

            # Print to file
            if fd is not None:
                self.__print_error_to_file(fd, i, error, min_error, epoch)

            # Advance iterator
            i += 1

        self.weights = min_weights

        # Storing x, y & error values
        self.error = min_error

        # print("Epoch", epoch, ": Iterated", i, "times")
        # print("Stopped because:", end="\t")
        if min_error == 0:
            # print("Error is 0")
            pass
        else:
            pass
            # print("Reached max iterations")
        return

    # Receives 2D array and returns a new array with predicted value for each x
    def test(self, x: np.ndarray, weights: np.ndarray = None) -> np.ndarray:

        # Adding bias column
        x_wb = np.insert(x, 0, values=1, axis=1)

        # Using self weight if it isn't specified
        w = weights
        if w is None:
            w = self.weights

        # check if weights is updated through train
        if w is None:
            raise Exception("train method must be called beforehand")

        # activate each excited value for each input
        y = np.fromiter(map(lambda x_i: self._activation(self._excitation(x_i, w)), x_wb), dtype=float)

        return y

    # Activation method, which depends on perceptron type
    @staticmethod
    def _activation(h):
        # Returns the sign of h
        return -1 if h < 0 else 1

    # Derivative of the activation function
    @staticmethod
    def _activation_derivative(h):
        # In this case derivative would be ignored so 1
        return 1

    # weighted sum
    @staticmethod
    def _excitation(x_i, w):
        # dot does the same thing as sum(x_i * self.weights)
        return np.dot(x_i, w)

    # Public excitation
    def excitation(self, x: np.ndarray):
        # We use saved weights
        w = self.weights
        # Sanity check
        if w is None:
            raise Exception("Update weights before calling activation method.")
        # We get excitation
        return self._excitation(x, w)

    # Public activation
    def activation(self, h):
        # We get activation and return it
        return self._activation(h)

    # Public activation derivative
    def activation_derivative(self, h):
        return self._activation_derivative(h)

    # delta_w, which depends on Perceptron implementation and activation
    def _get_delta_w(self, x, y, weights, index):
        # Excitation step, the weighting sum
        excitation = self._excitation(x[index], weights)
        # Activation step, a function to get a value from the excited values
        activation = self._activation(excitation)
        # Derivative of the activation function
        derivative = self._activation_derivative(excitation)
        # Calculating delta_w
        return self.lr * (y[index] - activation) * derivative * x[index]

    # Calculates error based on distance to real value, y is expected value
    def _get_error(self, x, y, w):

        # Calculate the value
        predicted_value = self.test(x, w)
        # Compare calculated value with testing one, abs to accumulate error values
        return sum(abs(np.subtract(y, predicted_value)))

    # Public get error
    def get_error(self, x, y):
        # Weights
        w = self.weights
        if w is None:
            raise Exception("Call to perceptron without weights.")
        return self._get_error(x, y, w)

    # Calculate accuracy
    def accuracy(self, x: np.ndarray, y: np.ndarray, min_error: float = 0.001):
        # Calculate the values
        predicted_values = self.test(x)
        # We initialize variables
        t = f = 0
        # We calculate the variables
        if len(predicted_values) != len(y):
            raise Exception("Something went wrong")
        for i in range(len(predicted_values)):
            # If they match then it's a true something
            if abs(predicted_values[i] - y[i]) < min_error:
                t += 1
            else:
                f += 1
        # We make sure data makes sense
        if t + f != len(y):
            raise Exception("Trues + False was not same size as dataset")
        # We return accuracy
        return float(t) / (f + t)

    # Resets weights
    def reset_weights(self):
        self.weights = None
        self.error = None

    # Output the error value to a fd
    @staticmethod
    def __print_error_to_file(fd, iteration, current_error, min_error, epoch):
        fd.write(
            str(epoch) + "\t" +
            str(iteration) + "\t" +
            str(current_error) + "\t" +
            str(min_error) + "\n"
        )
