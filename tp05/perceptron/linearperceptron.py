import numpy as np
from .perceptron import Perceptron


class LinearPerceptron(Perceptron):
    key = "linear-with-one-entry-delta-w"

    # Activation method, which depends on perceptron type
    @staticmethod
    def _activation(h):
        # Returns the weighted sum, which is equal to the excitation output
        return h

    # Derivative of activation function
    @staticmethod
    def _activation_derivative(h):
        # Derivative of g(x) = x function
        return 1

    # Error calculation y is expected values
    def _get_error(self, x, y, w):

        # Calculate the value
        predicted_values = self.test(x, w)
        # Compare calculated value with testing one using E(w) for linear perceptron
        return 0.5 * sum(np.square(np.subtract(y, predicted_values).astype(np.longdouble)))
