from .nonlinearperceptron import NonLinearPerceptron
import numpy as np


class NonLinearPerceptronAllEntries(NonLinearPerceptron):
    key = "non-linear-with-all-entries-delta-w"

    # delta_w, which depends on Perceptron implementation and activation
    def _get_delta_w(self, x, y, weights, index):
        # Number of elements in training set
        p = len(y)

        # Array with the gradient
        gradient = np.array(list(map(
            lambda i: (y[i] - self._activation(self._excitation(x[i], weights))) * self._activation_derivative(
                self._excitation(x[i], weights)) * x[i], range(p)
        )))

        # lr * gradient which approaches to the lowest Error value
        return self.lr * sum(gradient)
